Isa 40:31 \ Any good thing which has to be waited for comes with it better things that you will not have to wait for
Hab 2:3  
James 1:4 
Ec 7:8\ What is to be waited for comes with it a reward worth waiting for
Rom 2:7
Ps 37:9\Patience is the key to successful governance on this earth because this earth is governed by time and patience is the evidence of the mastery of time