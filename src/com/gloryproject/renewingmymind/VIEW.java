package com.gloryproject.renewingmymind;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

import com.gloryproject.renewingmymind.R;

import android.app.Activity;
import android.app.Dialog;
import android.app.FragmentManager.OnBackStackChangedListener;
import android.content.Intent;
import android.os.Bundle;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.text.util.Linkify;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class VIEW extends Activity {
	String selected;
@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.devotionalview);
	
	
	
	final TextView title=(TextView) findViewById(R.id.devotitle);
	
	final TextView view=(TextView) findViewById(R.id.devodisplay);
	
	
	String thetitle=getIntent().getStringExtra("TITLE");
	String theview=getIntent().getStringExtra("VIEW");
	String aboutview=getIntent().getStringExtra("ABOUTUS");
	if(thetitle.equals("ABOUT US")){
		title.setText(thetitle);
		view.setText(aboutview);
	}else{
	title.setText(thetitle);
	String[] devotion=theview.split("\n");
	int numbernewline=devotion.length;
	
	view.setText(theview);
	
	Pattern pattern = Pattern.compile("[1-3]+[a-zA-Z]+\\s+\\d+:+\\d+\\d");
	Pattern pattern0=Pattern.compile("[a-zA-Z]+\\s+\\d+:+\\d+\\d");
	Pattern pattern1=Pattern.compile("[a-zA-Z]+\\s+\\d+:+\\d");
	Pattern pattern2=Pattern.compile("[1-3]+[a-zA-Z]+\\s+\\d+:+\\d");
	Pattern pattern3 = Pattern.compile("[1-3]+[a-zA-Z]+\\s+\\d+\\d+:+\\d+\\d");
	Pattern pattern4 = Pattern.compile("[1-3]+[a-zA-Z]+\\s+\\d+\\d+:+\\d");
	Pattern pattern5=Pattern.compile("[a-zA-Z]+\\s+\\d+\\d+:+\\d");
	Pattern pattern6=Pattern.compile("[a-zA-Z]+\\s+\\d+\\d+:+\\d+\\d");
	
	Pattern pattern7=Pattern.compile("[a-zA-Z]+\\s+\\d+\\d+\\d+:+\\d");
	Pattern pattern8=Pattern.compile("[a-zA-Z]+\\s+\\d+\\d+\\d+:+\\d+\\d");
	Pattern pattern9=Pattern.compile("[a-zA-Z]+\\s+\\d+\\d+\\d+:+\\d+\\d+\\d+");

	Linkify.addLinks(view,pattern , "com.package.name://action-to-perform/id-that-might-be-needed/");
	
	Linkify.addLinks(view,pattern0 , "com.package.name://action-to-perform/id-that-might-be-needed/");
	Linkify.addLinks(view,pattern1 , "com.package.name://action-to-perform/id-that-might-be-needed/");
	Linkify.addLinks(view,pattern2 , "com.package.name://action-to-perform/id-that-might-be-needed/");
	Linkify.addLinks(view,pattern3 , "com.package.name://action-to-perform/id-that-might-be-needed/");
	Linkify.addLinks(view,pattern4 , "com.package.name://action-to-perform/id-that-might-be-needed/");
	Linkify.addLinks(view,pattern5 , "com.package.name://action-to-perform/id-that-might-be-needed/");
	Linkify.addLinks(view,pattern6 , "com.package.name://action-to-perform/id-that-might-be-needed/");
	
	Linkify.addLinks(view,pattern7 , "com.package.name://action-to-perform/id-that-might-be-needed/");
	Linkify.addLinks(view,pattern8 , "com.package.name://action-to-perform/id-that-might-be-needed/");
	Linkify.addLinks(view,pattern9 , "com.package.name://action-to-perform/id-that-might-be-needed/");

	
	
	view.setMovementMethod(new ScrollingMovementMethod());
	view.setMovementMethod(LinkMovementMethod.getInstance());
	
	}
	LinearLayout mylayout = (LinearLayout) findViewById(R.id.MYVIEW);
	mylayout.getBackground().setAlpha(50);
	
	view.setOnClickListener(new View.OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			boolean key=false;
			Dialog d=new Dialog(VIEW.this);
			d.setContentView(R.layout.menudialog);
			final TextView tv=(TextView)d.findViewById(R.id.menudisplay);
			final Button okbutton=(Button) d.findViewById(R.id.menuok);
			
			if(view.getSelectionStart()== -1 && 
                    view.getSelectionEnd() == -1){
                //Toast.makeText(getApplicationContext(), "You clicked outside the link", 
                        //Toast.LENGTH_SHORT).show();
            }
            else {

                int start = view.getSelectionStart();
                int end = view.getSelectionEnd();
                selected = view.getText().toString().substring(start, end);
               
                try {
                	Intent i=new Intent(VIEW.this,Biblepopup.class);
                	 tv.setText(readBible(selected));
                	 //d.show();
                	 i.putExtra("VERSE", readBible(selected));
                	 startActivity(i);
					Toast.makeText(getApplicationContext(), 
					        "Clicked: " + readBible(selected), 
					        
					        Toast.LENGTH_LONG).show();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
              key=true;
              
             
             
                
            }
			
			
		}
	});
	
}
@Override
	protected void onResume() {
		super.onResume();
		LinearLayout mylayout = (LinearLayout) findViewById(R.id.MYVIEW);
		mylayout.getBackground().setAlpha(50);
	}
public  String readBible(String expectedLine) throws IOException {
	if(expectedLine.contains(",")){expectedLine=expectedLine.replaceAll(",", "");}

	if(expectedLine.contains(".")){
	expectedLine=expectedLine.substring(expectedLine.indexOf(".")+1);
	}
	if(expectedLine.contains(")")){
		expectedLine=expectedLine.substring(expectedLine.indexOf(")")+1);
		}
	allocation allot=new allocation();
	String bibleverse=allot.books(expectedLine);
	String fullverse="";
	if(bibleverse.contains("-")){
		fullverse=bible(bibleverse.substring(0,bibleverse.indexOf(".")), 
				bibleverse.substring(bibleverse.indexOf(".")+1,bibleverse.indexOf(":")), 
				bibleverse.substring(bibleverse.indexOf(":")+1,bibleverse.indexOf("-")),
				bibleverse.substring(bibleverse.indexOf("-")+1));	
	}else{
		fullverse=bible(bibleverse.substring(0,bibleverse.indexOf(".")), 
				bibleverse.substring(bibleverse.indexOf(".")+1,bibleverse.indexOf(":")), 
				bibleverse.substring(bibleverse.indexOf(":")+1),
				"");
	}
	return fullverse;
}
public  String bible(String BOOK,String CHAPTER,String VERSE,String END) throws IOException{
	InputStream inputStream = this.getResources().getAssets().open(BOOK+".txt");
	BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	
	int linecount=0;
	String expectedLine="";
	String line="";
	int counter=0;
	int limiter=0;
	boolean begin=false;
	 boolean endverse=false;
if(!END.equals("")){limiter=Integer.parseInt(END)-Integer.parseInt(VERSE);}
	 while ((line = reader.readLine()) != null) {
     	linecount++;
       if(BOOK.toLowerCase().contains("psal")){
     	  
     	  if(line.contains("PSALM "+CHAPTER))
 {   
     		  begin=true;
 }
     		  if(begin==true){
     		  if(line.trim().startsWith(VERSE+" ")){
     			  
	 expectedLine=BOOK+CHAPTER+":"+line;break;
	 
	 
     		  }
 }

     		  
     	      }  
       else{
       if(line.contains("CHAPTER "+CHAPTER))
       {   
    	 begin=true;  
    	  //expectedLine=line;break; 
       
     }
      
       if(begin==true){
       if(line.trim().startsWith(VERSE+" ")||endverse==true)
       {
    	   if(END.equals(""))
    	   {expectedLine=BOOK+CHAPTER+":"+line;break;}
    	   else
    	   {
    		   endverse=true;
    		   expectedLine=expectedLine+line;
    		  counter++;
    		  if(counter==limiter+1){
    			  expectedLine=BOOK+CHAPTER+":"+expectedLine;
    			  begin=false;break;
    		  }
    	   }
    		   
    	   
       }
       
       }
	 }
}
	 return expectedLine;	 
}



}
