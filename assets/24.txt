24a)Mt 18:3 And said, Verily I say unto you, Except ye be converted, and become as little children, ye shall not enter into the kingdom of heaven.\God will not do anything if you will not convert.
24b)Mr 1:15 And saying, The TIME IS FULFILLED, and the kingdom of God is at hand: repent ye, and believe the gospel. God has already done all that is necessary.
24c)Mr 16:16 He that believeth and is baptized shall be saved; but he that believeth not shall be damned.
24d)1Ti 2:4 Who WILL HAVE ALL MEN to be saved, and to come unto the knowledge of the truth. \God not only wants you to be saved but commands you to believe and be saved.
24e)2Pe 3:9 The Lord is not slack concerning his promise, as some men count slackness; but IS LONGSUFFERING to us-ward, not willing that any should perish, but that all should come to repentance. \God has gone to all extremes to ensure your salvation.
24f)Re 22:17 And the Spirit and the bride say, Come. And let him that heareth say, Come. \And let him that is athirst come. And whosoever WILL, let him take the water of life freely. All that is left is in your will, in your power.
