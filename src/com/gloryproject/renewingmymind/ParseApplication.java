package com.gloryproject.renewingmymind;

import android.app.Application;

import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseUser;

public class ParseApplication extends Application {

  @Override
  public void onCreate() {
    super.onCreate();

    // Add your initialization code here
    Parse.initialize(this, "e5sayD2kSIpaEuB5lqmIjS7sX9v6uihOVBAkUAhN", "Prg8islTdiOwbjmdC1G1hIynRss6RgsVUjk84M1S");


    ParseUser.enableAutomaticUser();
    ParseACL defaultACL = new ParseACL();
      
    // If you would like all objects to be private by default, remove this line.
    defaultACL.setPublicReadAccess(true);
    
    ParseACL.setDefaultACL(defaultACL, true);
    
    ParseUser user=new ParseUser();
  }
}
