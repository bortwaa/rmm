package com.gloryproject.renewingmymind;



import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

public class Receiver extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent arg1) {
		String transclip=arg1.getStringExtra("theclip");
		Intent i=new Intent(context,PopupBible.class);
		i.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
		
		//transclip=Bibleallocation.books(transclip);
		i.putExtra("postclip", transclip);
		context.startActivity(i);

	}

}
