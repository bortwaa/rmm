package com.gloryproject.renewingmymind;

import com.gloryproject.renewingmymind.R;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

public class SPLASHSCREEN extends Activity {
@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.splashscreen);
	
	Thread timer=new Thread(){
		public void run(){
			try {
				synchronized (this) {
					wait(3000);
				}
			} catch (InterruptedException e) {
				e.printStackTrace();
			}finally{
				Intent i=new Intent(SPLASHSCREEN.this,MENU.class);
				startActivity(i);
			}
		}
	};timer.start();
}
@Override
	protected void onPause() {
		super.onPause();
		finish();
	}
}
