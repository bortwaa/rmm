199a)Pr 4:18 But the path of the just is as the shining light, that shineth more and more unto the perfect day. \The Christian life I live is an ever-glorious walk.
199b)Mr 11:23 For verily I say unto you, That whosoever shall say unto this mountain, Be thou removed, and be thou cast into the sea; and shall not doubt in his heart, but shall believe that those things which he saith shall come to pass; he shall have whatsoever he saith. \My condition is at the mercy of my faith.
199c)2Co 3:8 How shall not the ministration of the spirit be rather glorious
199d)Heb 6:1  Therefore leaving the principles of the doctrine of Christ, let us go on unto perfection; not laying again the foundation of repentance from dead works, and of faith toward God. \God desires to bring me to perfect conditions through faith toward God and repentance or a change of attitude towards my conditions
199e)Php 3:13 Brethren, I count not myself to have apprehended: but this one thing I do, forgetting those things which are behind, and reaching forth unto those things which are before. \There is always something ahead that I have to reach for.
199f)Jas 1:17 Every good gift and every perfect gift is from above, and cometh down from the Father of lights, with whom is no variableness, neither shadow of turning. \God is for me. This means that all good things are for me.
199g)Jer 29:11 For I know the thoughts that I think toward you, saith the LORD, thoughts of peace, and not of evil, to give you an expected end.  
199h)2Pe 1:3 According as his divine power hath given unto us all things that pertain unto life and godliness, through the knowledge of him that hath called us to glory and virtue: 
199i)2Co 9:8 And God is able to make all grace abound toward you; that ye, always having all sufficiency in all things, may abound to every good work:
199j)Eph 3:20 Now unto him that is able to do exceeding abundantly above all that we ask or think, according to the power that worketh in us,
199k)Isa 54:10 For the mountains shall depart, and the hills be removed; but my kindness shall not depart from thee, neither shall the covenant of my peace be removed, saith the LORD that hath mercy on thee.
199l)Hag 2:9 The glory of this latter house shall be greater than of the former. \The best conditions are ahead.
199m)Heb 10:35 Cast not away therefore your confidence, which hath great recompence of reward. 36 For ye have need of patience, that, after ye have done the will of God, ye might receive the promise.
199n)2Pe 1:3 According as his divine power hath given unto us all things that pertain unto life and godliness, through the knowledge of him that hath called us to glory and virtue: 
