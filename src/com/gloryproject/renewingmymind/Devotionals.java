package com.gloryproject.renewingmymind;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;

import android.app.Dialog;
import android.app.ListActivity;
import android.content.Intent;
import android.content.res.AssetManager;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseAnalytics;
import com.parse.ParseInstallation;
import com.parse.PushService;

public class Devotionals extends ListActivity {
	
@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
    Parse.initialize(this, "e5sayD2kSIpaEuB5lqmIjS7sX9v6uihOVBAkUAhN", "Prg8islTdiOwbjmdC1G1hIynRss6RgsVUjk84M1S");
    ParseInstallation.getCurrentInstallation().saveInBackground();
    
    
	AssetManager assetManager=getApplicationContext().getAssets();
	
	String devotionals[];
	try {
		devotionals = assetManager.list("articles");
		setListAdapter(new ArrayAdapter<String>(Devotionals.this, android.R.layout.simple_list_item_1, devotionals));

	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}


}
@Override
protected void onListItemClick(ListView l, View v, int position, long id) {
	super.onListItemClick(l, v, position, id);
	String record=l.getItemAtPosition(position).toString();
	String filename=record.trim();
	
	Intent i=new Intent(Devotionals.this,VIEW.class);
	i.putExtra("TITLE", record);
	try {
		i.putExtra("VIEW", readDevotion(l.getItemAtPosition(position).toString()));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	

	
		startActivity(i);
		


		//title.setText(record);
		//verseview.setText("this is it");
		
	
	//Toast.makeText(second.this, record, 5000).show();
}
public String readDevotion(String filename) throws IOException{
	InputStream inputStream = this.getResources().getAssets().open("articles/"+filename);
	BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	String line="";
    String expectedLine=""; 
        while ((line = reader.readLine()) != null) {
        	expectedLine=expectedLine+line+"\n";
        }
		return expectedLine;
}

@Override
protected void onPause() {
	super.onPause();
	finish();
}
}
