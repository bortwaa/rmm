107.Col 3:23 
Rev 22:13  \It is not about finishing but all about doing things unto the Lord who is the finisher as well as the starter of all things. Whatever is done unto the Lord always has a good start and a good finishing because the Lord is the beginning and the end of all things.
a.Col 3:23 
Rev 22:13 
Heb 12:2  \It is not about finishing but all about doing things unto the Lord who is the finisher as well as the starter of all things. Whatever is done unto the Lord always has a good start and a good finishing because the Lord is the beginning and the end of all things done in His name
