package com.gloryproject.renewingmymind;

import java.util.Iterator;

public class allocation {
    /**
     *
     */
	 String oldtestament="",
            newtestament="",
            law="",
            prophets="",
            gospels="",
            history="",
            poetic="",
            wisdom="",
            majorprophets="",
            minorprophets="",
            pauline="",
            letters="",
            inspiration="";
	public  String books(String biblereference){
        if(biblereference.toLowerCase().contains("1cor"))
        {
            biblereference=biblereference.toLowerCase().replace("1cor", "1Corinthians");
        }else
            if(biblereference.toLowerCase().contains("1co")){
                biblereference=biblereference.toLowerCase().replace("1co", "1Corinthians");
            }else
                if(biblereference.toLowerCase().contains("joh")){
                    biblereference=biblereference.toLowerCase().replace("joh", "John");
                }else
                    if(biblereference.toLowerCase().contains("john")){
                        biblereference=biblereference.toLowerCase().replace("john", "John");
                    }else
                        if(biblereference.toLowerCase().contains("lu")){
                            biblereference=biblereference.toLowerCase().replace("lu", "Luke");
                        }else
                            if(biblereference.toLowerCase().contains("2co")){
                                biblereference=biblereference.toLowerCase().replace("2co", "2Corinthians");
                            }else
                            if(biblereference.toLowerCase().contains("2cor")){
                                biblereference=biblereference.toLowerCase().replace("2cor", "1Corinthians");
                            }else
                                if(biblereference.toLowerCase().contains("song")){
                                    biblereference=biblereference.toLowerCase().replace("song", "SongofSolomon");
                                }else
                                    if(biblereference.toLowerCase().contains("jam")){
                                        biblereference=biblereference.toLowerCase().replace("jam", "James");
                                    }else
                                    	if(biblereference.toLowerCase().contains("jas")){
                                        biblereference=biblereference.toLowerCase().replace("jas", "James");
                                    }else
                                    	if(biblereference.toLowerCase().contains("ja")){
                                        biblereference=biblereference.toLowerCase().replace("ja", "James");
                                    }else
                                        if(biblereference.toLowerCase().contains("1th")){
                                            biblereference=biblereference.toLowerCase().replace("1th", "1Thessalonians");
                                        }else
                                            if(biblereference.toLowerCase().contains("2th")){
                                                biblereference=biblereference.toLowerCase().replace("2th", "2Thessalonians");
                                            }else
                                                if(biblereference.toLowerCase().contains("1ti")){
                                                    biblereference=biblereference.toLowerCase().replace("1ti", "1Timothy");
                                                }else
                                                    if(biblereference.toLowerCase().contains("1tim")){
                                                        biblereference=biblereference.toLowerCase().replace("1tim", "1Timothy");
                                                    }else
                                                        if(biblereference.toLowerCase().contains("2tim")){
                                                            biblereference=biblereference.toLowerCase().replace("2tim", "2Timothy");
                                                        }else
                                                            if(biblereference.toLowerCase().contains("2ti")){
                                                                biblereference=biblereference.toLowerCase().replace("2tim", "2Timothy");
                                                            }else
                                                                if(biblereference.toLowerCase().contains("1 th")){
                                                                    biblereference=biblereference.toLowerCase().replace("1 th", "1Thessalonians");
                                                                }else
                                                                    if(biblereference.toLowerCase().contains("2 th")){
                                                                        biblereference=biblereference.toLowerCase().replace("2 th", "2Thessalonians");
                                                                    }else
                                                                        if(biblereference.toLowerCase().contains("1 ti")){
                                                                            biblereference=biblereference.toLowerCase().replace("1 ti", "1Timothy");
                                                                        }else
                                                                            if(biblereference.toLowerCase().contains("1 tim")){
                                                                                biblereference=biblereference.toLowerCase().replace("1 tim", "1Timothy");
                                                                            }else
                                                                                if(biblereference.toLowerCase().contains("2 tim")){
                                                                                    biblereference=biblereference.toLowerCase().replace("2 tim", "2Timothy");
                                                                                }else
                                                                                    if(biblereference.toLowerCase().contains("2 ti")){
                                                                                        biblereference=biblereference.toLowerCase().replace("2 tim", "2Timothy");
                                                                                    }else
                                                                                        if(biblereference.toLowerCase().contains("song")){
                                                                                            biblereference=biblereference.toLowerCase().replace("Song", "SongofSolomon");
                                                                                        }
        
        
        String chapter = null;
        String verse=null;
        String verseend="";
        if(biblereference.contains("-")){
        verse=biblereference.substring(biblereference.indexOf("-")).trim();
        }
        if(biblereference.contains(".")){
            if(biblereference.substring(biblereference.indexOf(".")+1).contains(".")){
               biblereference=biblereference.substring(0, biblereference.indexOf("."))+
                       biblereference.substring(biblereference.indexOf(".")+1).replace(".", ":");
            }
        }
        //System.out.println(biblereference);
        if(biblereference.contains(" "))
        { biblereference=biblereference.replaceAll(" ", "");}
        
        String abbbook=biblereference.replaceAll("[^a-zA-Z]", "").toLowerCase();
        if(isanumber(String.valueOf(biblereference.trim().charAt(0))))
        {
            abbbook=String.valueOf(biblereference.trim().charAt(0))+abbbook;
        }
        //abbbook=bibleabbreviations(abbbook);
        //for the chapter
        if(biblereference.substring(abbbook.length()).contains(":"))
        {chapter=biblereference.substring(abbbook.length(),biblereference.indexOf(":"));
        //verse
        if(biblereference.contains("-"))
        {verse=biblereference.substring(biblereference.indexOf(":"),biblereference.indexOf("-"));
        verseend=biblereference.substring(biblereference.indexOf("-")+1);
        }else
        {verse=biblereference.substring(biblereference.indexOf(":")+1);}   
        
        }
        if(biblereference.substring(abbbook.length()).contains(".")&biblereference.substring(abbbook.length()+1).indexOf(".")>0)
        {chapter=biblereference.substring(abbbook.length(),biblereference.indexOf("."));}
        if(chapter.trim().startsWith("."))
                {
                chapter=chapter.substring(1);
                }
        //for the verse
       // verseend=String.valueOf(Integer.parseInt(verse)+2);
        chapter=chapter.replaceAll("[^\\d]", "");
        verse=verse.replaceAll("[^\\d]", "");
        //System.out.println(chapter);
        //System.out.println(verse);
        boolean IsItTheRightBook=false;
        String rightbook=null;
        //Scan through all the nonabbbook to find matches
        for (int i = 0; i < nonabbbook.length; i++) {
nonabbbook[i]=nonabbbook[i].toLowerCase();
                //Determine beginning letter and check if the characters are contained
            if(abbbook.startsWith(nonabbbook[i].substring(0, 1))&matches(abbbook,nonabbbook[i])){
                       //System.out.println(nonabbbook[i]); 
                //System.out.println(abbbook.indexOf("i")+"this is it");
                //System.out.println(abbbook.length());
            	if(biblereference.toLowerCase().contains("joh")){
                   rightbook="John";
                }else
                if(biblereference.toLowerCase().contains("john")){
                    rightbook="John";
                }else
                       if(abbbook.length()<12){
                           rightbook=nonabbbook[i];
                           IsItTheRightBook=true;
                           
                           
                       }else
                       {
                        //Check the order of the letters in abbreviation
                    for (int j = 1; j < abbbook.length()-2; j++) {
                        //System.out.println(nonabbbook[i].substring(nonabbbook[i].indexOf(abbbook.charAt(j-1))));
                      
                            if (nonabbbook[i].substring(nonabbbook[i].indexOf(abbbook.charAt(j-1))).indexOf(abbbook.charAt(j))
                                    <
                                nonabbbook[i].substring(nonabbbook[i].indexOf(abbbook.charAt(j-1))).indexOf(abbbook.charAt(j+1))    
                                    ) {
                                rightbook=nonabbbook[i];
                                IsItTheRightBook=true;
                            }else{IsItTheRightBook=false;}
                    }
                       
                   if(IsItTheRightBook){break;}
}
            
            }
     if(IsItTheRightBook){break;}
            
        }
if(isanumber(rightbook.trim().substring(0, 1)))
{
    rightbook=rightbook.trim().substring(0, 2).toUpperCase()+rightbook.trim().substring(2);}
else{rightbook=rightbook.trim().substring(0, 1).toUpperCase()+rightbook.trim().substring(1);}

if(biblereference.toLowerCase().contains("1co")){
    biblereference=biblereference.toLowerCase().replace("1co", "1Corinthians");
}

String everything=rightbook+"."+chapter+":"+verse;
if(biblereference.contains("-")){everything=everything+"-"+verseend;}

    
       return everything;
    		 
        
    }
    private static boolean matches(String abbbook, String string) {
    	boolean righteous=true;
		for (int i = 0; i < abbbook.length(); i++) {
			if(string.toLowerCase().contains(String.valueOf(abbbook.charAt(i)).toLowerCase())){
				righteous=true;
			}else{righteous=false;break;}
			
		}
    	
		return righteous;
	}
	public static boolean isanumber(String reference){
        boolean yes=true;
        try {
            int number=Integer.parseInt(reference);
        } catch (Exception e) {
            yes=false;
        }
        return yes;
    }	
public static String crossrefBOOKS[]= 
{
"Gen",
"Exod",
"Lev",
"Num",
"Deut",
"Josh",
"Judg",
"Ruth",
"1Sam",
"2Sam",
"1Kgs",
"2Kgs",
"1Chr",
"2Chr",
"Ezra",
"Neh",
"Esth",
"Job",
"Ps",
"Prov",
"Eccl",
"Song",
"Isa",
"Jer",
"Lam",
"Ezek",
"Dan",
"Hos",
"Joel",
"Amos",
"Obad",
"Jonah",
"Mic",
"Nah",
"Hab",
"Zeph",
"Hag",
"Zech",
"Mal",
"Matt",
"Mark",
"Luke",
"John",
"Acts",
"Rom",
"1Cor",
"2Cor",
"Gal",
"Eph",
"Phil",
"Col",
"1Thess",
"2Thess",
"1Tim",
"2Tim",
"Titus",
"Phlm",
"Heb",
"Jas",
"1Pet",
"2Pet",
"1John",
"2John",
"3John",
"Jude",
"Rev"

};    
public static String BOOKS[]= 
{
"Gen",
"Exo",
"Lev",
"Num",
"Deu",
"Jos",
"Jug",
"Rut",
"1Sa",
"2Sa",
"1Ki",
"2Ki",
"1Ch",
"2Ch",
"Ezr",
"Neh",
"Est",
"Job",
"Psa",
"Pro",
"Ecc",
"SoS",
"Isa",
"Jer",
"Lam",
"Eze",
"Dan",
"Hos",
"Joe",
"Amo",
"Oba",
"Jon",
"Mic",
"Nah",
"Hab",
"Zep",
"Hag",
"Zec",
"Mal",
"Mat",
"Mar",
"Luk",
"Joh",
"Act",
"Rom",
"1Co",
"2Co",
"Gal",
"Eph",
"Php",
"Col",
"1Th",
"2Th",
"1Ti",
"2Ti",
"Tit",
"Phi",
"Heb",
"Jam",
"1Pe",
"2Pe",
"1Jo",
"2Jo",
"3Jo",
"Jud",
"Rev"
};

public static int NoChapterbooks[]= 
{
50,
40,
27,
36,
34,
24,
21,
4,
31,
24,
22,
25,
29,
36,
10,
13,
10,
42,
150,
31,
12,
8,
66,
52,
5,
48,
12,
14,
3,
9,
1,
4,
7,
3,
3,
3,
2,
14,
4,
28,
16,
24,
21,
28,
16,
16,
13,
6,
6,
4,
4,
5,
3,
6,
4,
3,
1,
13,
5,
5,
3,
5,
1,
1,
1,
22
}; 
public static String nonabbbook[]={
"Genesis",
"Exodus",
"Leviticus",
"Numbers",
"Deuteronomy",
"Joshua",
"Judges",
"Ruth",
"1Samuel",
"2Samuel",
"1Kings",
"2Kings",
"1Chronicles",
"2Chronicles",
"Ezra",
"Nehemiah",
"Esther",
"Job",
"Psalms",
"Proverbs",
"Ecclesiastes",
"Song of Songs",
"Isaiah",
"Jeremiah",
"Lamentations",
"Ezekiel",
"Daniel",
"Hosea",
"Joel",
"Amos",
"Obadiah",
"Jonah",
"Micah",
"Nahum",
"Habakkuk",
"Zephaniah",
"Haggai",
"Zechariah",
"Malachi",
"Matthew",
"Mark",
"Luke",
"John",
"Acts",
"Romans",
"1Corinthians",
"2Corinthians",
"Galatians",
"Ephesians",
"Philippians",
"Colossians",
"1Thessalonians",
"2Thessalonians",
"1Timothy",
"2Timothy",
"Titus",
"Philemon",
"Hebrews",
"James",
"1Peter",
"2Peter",
"1John",
"2John",
"3John",
"Jude",
"Revelation"

};
public  String bibleabbreviations(String myabbreviation)
{
    String bibleabbreviation=myabbreviation.trim();
//    bibleabbreviation=bibleabbreviation.substring(0, 1).toUpperCase()+bibleabbreviation.substring(1, bibleabbreviation.length());
    String biblebook=null;
    if (bibleabbreviation.startsWith("G".toLowerCase()))
    {
        biblebook="Genesis";
        oldtestament="oldtestament";
            newtestament="";
            law="law";
            prophets="";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Ex".toLowerCase()))
    {
        biblebook="Exodus";
        oldtestament="oldtestament";
            newtestament="";
            law="law";
            prophets="";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Le".toLowerCase()))
    {
        biblebook="Leviticus";
        oldtestament="oldtestament";
            newtestament="";
            law="law";
            prophets="";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Nu".toLowerCase()))
    {
        biblebook="Numbers";
        oldtestament="oldtestament";
            newtestament="";
            law="law";
            prophets="";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("De".toLowerCase()))
    {
        biblebook="Deuteronomy";
        oldtestament="oldtestament";
            newtestament="";
            law="law";
            prophets="";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Jos".toLowerCase())
            
            )
    {
        biblebook="Joshua";
            oldtestament="oldtestament";
            newtestament="";
            law="law";
            prophets="";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Jug".toLowerCase())||bibleabbreviation.startsWith("Jud".toLowerCase())||bibleabbreviation.startsWith("Jg".toLowerCase()))
    {
        biblebook="Judges";
         oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Ru".toLowerCase()))
    {
        biblebook="Ruth";
         oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("1S".toLowerCase()))
    {
        biblebook="1Samuel";
         oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
     if (bibleabbreviation.startsWith("2S".toLowerCase()))
    {
        biblebook="2Samuel";
         oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("1K".toLowerCase()))
    {
        biblebook="1Kings";
            oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("2K".toLowerCase()))
    {
        biblebook="2Kings";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("1Ch".toLowerCase()))
    {
        biblebook="1Chronicles";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("2Ch".toLowerCase()))
    {
        biblebook="2Chronicles";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Ezr".toLowerCase())||bibleabbreviation.startsWith("Eza".toLowerCase()))
    {
        biblebook="Ezra";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Ne".toLowerCase()))
    {
        biblebook="Nehemiah";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Es".toLowerCase()))
    {
        biblebook="Esther";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.contains("Job".toLowerCase())||(bibleabbreviation.startsWith("Jb".toLowerCase())))
    {
        biblebook="Job";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="";
            gospels="";
            history="history";
            poetic="poetic";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="inspiration";
    }
    if (bibleabbreviation.startsWith("Ps".toLowerCase()))
    {
        biblebook="Psalms";
            oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="poetic";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="inspiration";
    }
    if (bibleabbreviation.startsWith("Pr".toLowerCase()))
    {
        biblebook="Proverbs";
            oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="poetic";
            wisdom="wisdom";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="inspiration";
    }
    if (bibleabbreviation.startsWith("Ec".toLowerCase()))
    {
        biblebook="Ecclesiastes";
            oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="poetic";
            wisdom="wisdom";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="inspiration";
    }
    if (bibleabbreviation.startsWith("S".toLowerCase()))
    {
        biblebook="SongofSolomon";
         oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="poetic";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="inspiration";
    }
    if (bibleabbreviation.startsWith("I".toLowerCase()))
    {
        biblebook="Isaiah";
            oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="prophets";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="majorprophets";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Je".toLowerCase()))
    {
        biblebook="Jeremiah";
            oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="prophets";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="majorprophets";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("La".toLowerCase()))
    {
        biblebook="Lamentation";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="prophets";
            gospels="";
            history="";
            poetic="poetic";
            wisdom="";
            majorprophets="majorprophets";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="inspiration";
    }
    if (bibleabbreviation.startsWith("Ez".toLowerCase()))
    {
        biblebook="Ezekiel";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="prophets";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="majorprophets";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Da".toLowerCase()))
    {
        biblebook="Daniel";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="prophets";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="majorprophets";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Ho".toLowerCase()))
    {
        biblebook="Hosea";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="prophets";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="minorprophets";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Joe".toLowerCase())|| bibleabbreviation.startsWith("Jl".toLowerCase()))
    {
        biblebook="Joel";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="prophets";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="minorprophets";
            pauline="";
            letters="";
            inspiration="";
    }
     if (bibleabbreviation.startsWith("Am".toLowerCase()))
    {
        biblebook="Amos";
         oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="prophets";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="minorprophets";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Ob".toLowerCase()))
    {
        biblebook="Obadiah";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="prophets";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="minorprophets";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Jna".toLowerCase())||bibleabbreviation.startsWith("Jnh".toLowerCase())||bibleabbreviation.startsWith("Jon".toLowerCase()))
    {
        biblebook="Jonah";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="prophets";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="minorprophets";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Mi".toLowerCase()))
    {
        biblebook="Micah";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="prophets";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="minorprophets";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Na".toLowerCase()))
    {
        biblebook="Nahum";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="prophets";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="minorprophets";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Ha".toLowerCase()))
    {
        biblebook="Habakkuk";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="prophets";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="minorprophets";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Ze".toLowerCase())||bibleabbreviation.startsWith("Zep".toLowerCase()))
    {
        biblebook="Zephaniah";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="prophets";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="minorprophets";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Hag".toLowerCase()))
    {
        biblebook="Haggai";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="prophets";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="minorprophets";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.contains("Zec".toLowerCase())||bibleabbreviation.startsWith("Zc".toLowerCase()))
    {
        biblebook="Zechariah";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="prophets";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="minorprophets";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Mal".toLowerCase())||bibleabbreviation.startsWith("Ml".toLowerCase()))
    {
        biblebook="Malachi";
        oldtestament="oldtestament";
            newtestament="";
            law="";
            prophets="prophets";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="minorprophets";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Mat".toLowerCase())||bibleabbreviation.startsWith("Mt".toLowerCase()))
    {
        biblebook="Matthew";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="gospels";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Mar".toLowerCase())||bibleabbreviation.startsWith("Mr".toLowerCase()))
    {
        biblebook="Mark";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="gospels";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Luk".toLowerCase())||bibleabbreviation.startsWith("Lu".toLowerCase())||bibleabbreviation.startsWith("Lk".toLowerCase()))
    {
        biblebook="Luke";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="gospels";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="letters";
            inspiration="";
    }
    if (bibleabbreviation.contains("Joh".toLowerCase()))
    {
        biblebook="John";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="gospels";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Ac".toLowerCase()))
    {
        biblebook="Acts";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="history";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="letters";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Ro".toLowerCase())||bibleabbreviation.startsWith("Rm".toLowerCase()))
    {
        biblebook="Romans";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="pauline";
            letters="letters";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("1Co".toLowerCase()))
    {
        biblebook="1Corinthians";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="pauline";
            letters="letters";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("2Co".toLowerCase()))
    {
        biblebook="2Corinthians";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="pauline";
            letters="letters";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Ga".toLowerCase()))
    {
        biblebook="Galatians";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="pauline";
            letters="letters";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Ep".toLowerCase()))
    {
        biblebook="Ephesians";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="pauline";
            letters="letters";
            inspiration="";
    }
     if (bibleabbreviation.startsWith("Php".toLowerCase()))
    {
        biblebook="Philippians";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="pauline";
            letters="letters";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Co".toLowerCase()))
    {
        biblebook="Colossians";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="pauline";
            letters="letters";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("1Th".toLowerCase()))
    {
        biblebook="1Thessalonians";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="pauline";
            letters="letters";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("2Th".toLowerCase()))
    {
        biblebook="2Thessalonians";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="pauline";
            letters="letters";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("1Ti".toLowerCase()))
    {
        biblebook="1Timothy";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="pauline";
            letters="letters";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("2Ti".toLowerCase()))
    {
        biblebook="2Timothy";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="pauline";
            letters="letters";
            inspiration="";
    }
     if (bibleabbreviation.startsWith("Ti".toLowerCase())||bibleabbreviation.startsWith("Tit".toLowerCase()))
    {
        biblebook="Titus";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="pauline";
            letters="letters";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Phi".toLowerCase())||bibleabbreviation.startsWith("Phm".toLowerCase()))
    {
        biblebook="Philemon";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="pauline";
            letters="letters";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("He".toLowerCase()))
    {
        biblebook="Hebrews";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="pauline";
            letters="letters";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Ja".toLowerCase()))
    {
        biblebook="James";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="letters";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("1P".toLowerCase()))
    {
        biblebook="1Peter";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="letters";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("2P".toLowerCase()))
    {
        biblebook="2Peter";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="letters";
            inspiration="";
    }
      if (bibleabbreviation.startsWith("1J".toLowerCase()))
    {
        biblebook="1John";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="letters";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("2J".toLowerCase()))
    {
        biblebook="2John";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="letters";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("3J".toLowerCase()))
    {
        biblebook="3John";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="letters";
            inspiration="";
    }
    if (bibleabbreviation.startsWith("Jud".toLowerCase())||bibleabbreviation.startsWith("Jude".toLowerCase()))
    {
        biblebook="Jude";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="pauline";
            letters="letters";
            inspiration="";
            
    }
    if (bibleabbreviation.startsWith("Re".toLowerCase()))
    {
        biblebook="Revelation";
        oldtestament="";
            newtestament="newtestament";
            law="";
            prophets="";
            gospels="";
            history="";
            poetic="";
            wisdom="";
            majorprophets="";
            minorprophets="";
            pauline="";
            letters="letters";
            inspiration="";
    }
    
    return biblebook.toLowerCase();
} 
}