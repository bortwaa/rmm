25a)Jer 9:24 But let him that glorieth glory in this, that he understandeth and knoweth me, that I am the LORD which exercise �JUDGMENT�in the earth�
25b)Ex 20:5 Thou shalt not bow down thyself to them, nor serve them: for I the LORD thy God am a jealous God, VISITING THE iniquity of the fathers upon the children unto the third and fourth generation of them that hate me;
25c)Ro 12:19 �Vengeance is mine; I will repay, saith the Lord.
25d)Ps 94:2 Lift up thyself, thou judge of the earth: render a reward to the proud.
25e)Jer 21:14 But I will PUNISH you according to the fruit of your doings, saith the LORD: and I will kindle a fire in the forest thereof, and it shall devour all things round about it.
25f)Ps 9:8 And he shall judge the world in righteousness, he shall minister judgment to the people in uprightness.