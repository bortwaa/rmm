152a)Pr 1:32 For the turning away of the simple shall slay them, and the prosperity of fools shall destroy them.
152b)Ps 73:3 For I was envious at the foolish, when I saw the prosperity of the wicked...16 When I thought to know this, it was too painful for me; Until I went into the sanctuary of God; then understood I their end.
152c)Pr 10:22  The blessing of the LORD, it maketh rich, and he addeth no sorrow with it. \True prosperity is one without sorrows and this comes from God only.
152d)Mr 8:36 For what shall it profit a man, if he shall gain the whole world, and lose his own soul  \For every gain men make without God they lose a portion of their souls.
152e)Re 3:17 Because thou sayest, I am rich, and increased with goods, and have need of nothing; and knowest not that thou art wretched, and miserable, and poor, and blind, and naked:  \True prosperity is not rooted in the possession of things but in the possession of God; the creator of all things.
152f)Jer 9:23 Thus saith the LORD...let not the rich man glory in his riches: 24 But let him that glorieth glory in this, that he understandeth and knoweth me... \True prosperity is the spiritual knowledge of and walk with God
152g)Pr 14:12  There is a way which seemeth right unto a man, but the end thereof are the ways of death.
152h)Ec 2:8 I gathered me also silver and gold, and the peculiar treasure of kings and of the provinces:...11 Then I looked on all the works that my hands had wrought, and on the labour that I had laboured to do: and, behold, all was vanity and vexation of spirit, and there was no profit under the sun. \Prosperity without God is futility.
152i)Ec 2:26 For God giveth to a man that is good in his sight wisdom, and knowledge, and joy: but to the sinner he giveth travail, to gather and to heap up, that he may give to him that is good before God. This also is vanity and vexation of spirit. \In the end, people who prosper without God do so for the enjoyment of those who are with and for God. 

