package com.gloryproject.renewingmymind;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

import com.gloryproject.renewingmymind.R;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.ClipboardManager.OnPrimaryClipChangedListener;
import android.os.Build;
import android.os.Bundle;
import android.text.InputType;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class Bible extends ListActivity {
String book,chapter,verse,thechapter;
int position;
public EditText input;
public String searchable;
private int justifyverse;
@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	
	String[] parts=null;
	book=getIntent().getStringExtra("BOOK");
	int chapter=getIntent().getIntExtra("chapter", 1);
	position=getIntent().getIntExtra("BOOKPOSITION", 1);

    try {
		parts = readChapterFromBible(book.substring(0,book.indexOf(".")).trim()+".txt").split("\n");
	} catch (IOException e) {
		Toast.makeText(Bible.this, "wrong chapter", 8000).show();
		e.printStackTrace();
	}
    String first=parts[0];
    LinkedList<String> partlist=new LinkedList<String>(Arrays.asList(parts));
    partlist.remove(0);
    partlist.addFirst(parts[0].substring(4));
    
    String newpart[]=partlist.toArray(new String[partlist.size()]);
    
setListAdapter(new ArrayAdapter<String>(Bible.this,android.R.layout.simple_list_item_1,newpart));
getListView().setSelection(chapter);
getListView().setChoiceMode(ListView.CHOICE_MODE_SINGLE);



 
}
@Override
	protected void onListItemClick(ListView l, View v, int position, long id) {
		super.onListItemClick(l, v, position, id);
		 thechapter=l.getItemAtPosition(position).toString();
justifyverse=position;
		final Dialog d=new Dialog(Bible.this);
		d.setContentView(R.layout.myversetasks);
		d.setTitle(chapter);
		
		final Button crossref=(Button)d.findViewById(R.id.crossreference);
		
		if(thechapter.length()<25){
			String verse=thechapter.substring(0,thechapter.indexOf("."))+
					thechapter.substring(thechapter.indexOf(".")+1).replace(".", ":");
			if(thechapter.contains("-")){
				verse=thechapter.substring(0,thechapter.indexOf("."))+
						thechapter.substring(thechapter.indexOf(".")+1,thechapter.indexOf("-")).replace(".", ":");
				verse=verse+"-"+thechapter.substring(thechapter.lastIndexOf(".")+1);
			}
			Dialog myd=new Dialog(Bible.this);
			myd.setContentView(R.layout.dialogdisplay);
			myd.setTitle(thechapter);
			
			final TextView verseview=(TextView)myd.findViewById(R.id.versedisplay);
			

			verseview.setMovementMethod(new ScrollingMovementMethod());
			try {
				verseview.setText(readBible(verse));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			myd.show();
			
		}else{
		d.show();
		}
		crossref.setOnClickListener(new OnClickListener() {
			
			@Override
			public void onClick(View arg0) {
				
				{showcrossref();
				d.dismiss();
				}
				
			}
		});
		
		//Bible.this.finish();
		
		
	}
@Override
	public boolean onCreateOptionsMenu(Menu menu) {
	MenuInflater inflater=new MenuInflater(Bible.this);
	inflater.inflate(R.menu.biblemenu, menu);
		return true;
	}
@Override
	public boolean onOptionsItemSelected(MenuItem item) {
if(item.getItemId()==R.id.search){
	AlertDialog.Builder builder=new AlertDialog.Builder(Bible.this);
	
	{builder.setTitle("SEARCH WORDS IN THOUGHTS");}
	
	input=new EditText(Bible.this);
	input.setHint("type search key");
	
	input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
	builder.setView(input);
	
	builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
							
			try {
				
				{
					Toast.makeText(Bible.this, "testing", 5000).show();

					searchable=input.getText().toString();
					String searched[]=(searchBibleOutput().split("\n"));
					setListAdapter(new ArrayAdapter<String>(Bible.this, android.R.layout.simple_list_item_1, searched));
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	});
	builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			dialog.cancel();
			
		}
	});
	builder.show();;
	}
if(item.getItemId()==R.id.thismainmenu){
	Intent i=new Intent(Bible.this,MENU.class);
	startActivity(i);
}

	return super.onOptionsItemSelected(item);
	}
public String searchBibleFromAssets(String Biblebook,String searchkey) throws IOException{
	InputStream inputStream = this.getResources().getAssets().open(Biblebook);
	BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	
	String line="";
	String search=searchkey.toUpperCase();
    String expectedLine=""; 
        while ((line = reader.readLine()) != null) {
        	if(line.toUpperCase().contains(search.trim())){
        		 expectedLine=expectedLine+line+"\n";
            }
         
          
        }
		return expectedLine;
}
public String searchBibleOutput() throws IOException{
	String parts[]=null;
	String matches="";
	allocation allot=new allocation();
	for (int i = 0; i < 40; i++) {
		matches=matches+searchBibleFromAssets(allot.nonabbbook[i]+".txt",searchable)+"\n";
		
	}
	return matches;
}
public void showcrossref(){
	Dialog crossrefview=new Dialog(getApplicationContext());
	crossrefview.setContentView(R.layout.dialogdisplay);
	
	final TextView verseview=(TextView)crossrefview.findViewById(R.id.versedisplay);
	verseview.setMovementMethod(new ScrollingMovementMethod());
	
	//verseview.getBackground().setAlpha(50);
	
	crossrefview.setTitle(thechapter);
	crossrefview.setCancelable(true);
	
	String verseparts="";
	String verses=readline(book+"/"+position);
	verseview.setText(thechapter+"\n"+"\n"+verses);
	String parts[]=verses.split(",");
	
	String lineee="Phil.4.8	1Tim.3.7-1Tim.3.8";
    String line0=lineee.substring(8, 9);
	for (int i = 0; i < parts.length; i++) {
		verseparts=verseparts+parts[i]+",";
	}
	String theparts[]=verseparts.split("[\\s]+");
	String mostpart[]=null;
	List<String> partlist=new ArrayList<String>();
    for (String x : theparts) {
		partlist.add(x.substring(x.indexOf(",")+1));
	}
    
    
    String newpart[]=partlist.toArray(new String[partlist.size()]);
	Dialog mine=new Dialog(Bible.this);
	mine.setContentView(R.layout.mylistview);
    
	ListView mylist=(ListView) mine.findViewById(android.R.layout.simple_list_item_1);
	
	setListAdapter(new ArrayAdapter<String>(Bible.this,android.R.layout.simple_list_item_1 , newpart));

	
}

public String readChapterFromBible(String filename) throws IOException{
	InputStream inputStream = this.getResources().getAssets().open(filename);
	BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	String line="";
	int linecount=0;
	boolean b=false;
    String expectedLine=null; 
        while ((line = reader.readLine()) != null) {
        	linecount++;
          if(book.contains("Psal")){
        	  
        	  if(line.contains("PSALM "+book.substring(book.indexOf(".")+1, book.indexOf(":")))
        	        	||b==true	  
        	        		  )
        	          {expectedLine=expectedLine+line+"\n"; b=true;
        	          if(line.equals("")){b=false;break;}
        	          }  
          }else
          if(line.contains("CHAPTER "+book.substring(book.indexOf(".")+1, book.indexOf(":")))
        	||b==true	  
        		  )
          {   
        	  expectedLine=expectedLine+line+"\n"; b=true;
          if(line.equals("")){b=false;break;}
          }
          
        }
		return expectedLine;
}
public String readChapterFromAmpBible(String filename) throws IOException{
	InputStream inputStream = this.getResources().getAssets().open(filename);
	BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	String line="";
	int linecount=0;
	boolean b=false;
    String expectedLine=null; 
        while ((line = reader.readLine()) != null) {
        	linecount++;
          if(book.contains("Psal")){
        	  
        	  if(line.contains("PSALM "+book.substring(book.indexOf(".")+1, book.indexOf(":")))
        	        	||b==true	  
        	        		  )
        	          {expectedLine=expectedLine+line+"\n"; b=true;
        	          if(line.equals("")){b=false;break;}
        	          }  
          }else
          if(line.contains("CHAPTER "+book.substring(book.indexOf(".")+1, book.indexOf(":")))
        	||b==true	  
        		  )
          {   
        	  expectedLine=expectedLine+line+"\n"; b=true;
          if(line.equals("")){b=false;break;}
          }
          
        }
		return expectedLine;
}

public String readline(String myverse)
{
	int position=Integer.parseInt(myverse.substring(myverse.indexOf("/")+1));
	String fullbook=myverse.trim().substring(0, myverse.trim().indexOf("."));
	String ABBBOOK=allocation.crossrefBOOKS[position];
	String CHAPTER=myverse.trim().substring(myverse.trim().indexOf(".")+1, myverse.trim().indexOf(":"));
	String VERSE=String.valueOf(justifyverse);
   // String theVERSE=VERSE.substring(0, VERSE.indexOf("."));
	String verse=ABBBOOK+"."+CHAPTER+"."+VERSE;
	//String verse="Gen.5.3";
    String formatlines="";
    String line0="";
   String Crossbook="Cross"+fullbook;
     try {
     InputStream inputStream = this.getResources().getAssets().open(Crossbook+".txt");
        BufferedReader reader2 = new BufferedReader(new InputStreamReader(inputStream));
        String line,theline = null;
theline=String.valueOf(reader2.readLine());
        int line4 = 0;
        int chapter = 0;
        
        while ((line = reader2.readLine()) != null) {
            line4++;
            
            if (line.trim().startsWith(verse)) {
                String lineee="Phil.4.8	1Tim.3.7-1Tim.3.8";
                 line0=lineee.substring(8, 9);
//                System.out.println(line0);
//                System.out.println(line0.replaceFirst(line0, "________"));
                String lineformat=line.replaceFirst(line0, "________");
                
                String line1=lineformat.substring(lineformat.indexOf(".")+1);
                String line2=line1.substring(line1.indexOf(".")+4);
                
                String line3=line2.replaceAll("_", " ").trim();
                formatlines=formatlines+","+line3;
                System.out.println(line3);
                
                //System.out.println(line.trim().substring(0, line.indexOf(".")));
                //System.out.println(line);
            }
        }
}  catch (FileNotFoundException ex) {
       Logger.getLogger(Crossreference.class.getName()).log(Level.SEVERE, null, ex);
   } catch (IOException ex) {
       Logger.getLogger(Crossreference.class.getName()).log(Level.SEVERE, null, ex);
   }
return formatlines;
}
public  String readBible(String expectedLine) throws IOException {
	if(expectedLine.contains(",")){expectedLine=expectedLine.replaceAll(",", "");}

	if(expectedLine.contains(".")){
	expectedLine=expectedLine.substring(expectedLine.indexOf(".")+1);
	}
	if(expectedLine.contains(")")){
		expectedLine=expectedLine.substring(expectedLine.indexOf(")")+1);
		}
	allocation allot=new allocation();
	String bibleverse=allot.books(expectedLine);
	String fullverse="";
	if(bibleverse.contains("-")){
		fullverse=bible(bibleverse.substring(0,bibleverse.indexOf(".")), 
				bibleverse.substring(bibleverse.indexOf(".")+1,bibleverse.indexOf(":")), 
				bibleverse.substring(bibleverse.indexOf(":")+1,bibleverse.indexOf("-")),
				bibleverse.substring(bibleverse.indexOf("-")+1));	
	}else{
		fullverse=bible(bibleverse.substring(0,bibleverse.indexOf(".")), 
				bibleverse.substring(bibleverse.indexOf(".")+1,bibleverse.indexOf(":")), 
				bibleverse.substring(bibleverse.indexOf(":")+1),
				"");
	}
	return fullverse;
}
public  String bible(String BOOK,String CHAPTER,String VERSE,String END) throws IOException{
	InputStream inputStream = this.getResources().getAssets().open(BOOK+".txt");
	BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	
	int linecount=0;
	String expectedLine="";
	String line="";
	int counter=0;
	int limiter=0;
	boolean begin=false;
	 boolean endverse=false;
if(!END.equals("")){limiter=Integer.parseInt(END)-Integer.parseInt(VERSE);}
	 while ((line = reader.readLine()) != null) {
     	linecount++;
       if(BOOK.toLowerCase().contains("psal")){
     	  
     	  if(line.contains("PSALM "+CHAPTER))
 {   
     		  begin=true;
 }
     		  if(begin==true){
     		  if(line.trim().startsWith(VERSE+" ")){
     			  
	 expectedLine=BOOK+CHAPTER+":"+line;break;
	 
	 
     		  }
 }

     		  
     	      }  
       else{
       if(line.contains("CHAPTER "+CHAPTER))
       {   
    	 begin=true;  
    	  //expectedLine=line;break; 
       
     }
      
       if(begin==true){
       if(line.trim().startsWith(VERSE+" ")||endverse==true)
       {
    	   if(END.equals(""))
    	   {expectedLine=BOOK+CHAPTER+":"+line;break;}
    	   else
    	   {
    		   endverse=true;
    		   expectedLine=expectedLine+line;
    		  counter++;
    		  if(counter==limiter+1){
    			  expectedLine=BOOK+CHAPTER+":"+expectedLine;
    			  begin=false;break;
    		  }
    	   }
    		   
    	   
       }
       
       }
	 }
}
	 return expectedLine;	 
}

}
