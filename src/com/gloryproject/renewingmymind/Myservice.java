package com.gloryproject.renewingmymind;

import android.annotation.TargetApi;
import android.app.Service;
import android.content.ClipData;
import android.content.ClipboardManager;
import android.content.Intent;
import android.content.ClipboardManager.OnPrimaryClipChangedListener;
import android.os.Build;
import android.os.IBinder;
import android.widget.Toast;

@TargetApi(Build.VERSION_CODES.HONEYCOMB)
public class Myservice extends Service {

	@Override
	public IBinder onBind(Intent arg0) {
		// TODO Auto-generated method stub
		return null;
	}
	@Override
	public int onStartCommand(Intent intent, int flags, int startId) {
		
		final ClipboardManager clipboard=(ClipboardManager) getSystemService(CLIPBOARD_SERVICE);
        clipboard.addPrimaryClipChangedListener(new OnPrimaryClipChangedListener() {
			
			@Override
			public void onPrimaryClipChanged() {
				
				
				ClipData myclip=clipboard.getPrimaryClip();
				ClipData.Item item=myclip.getItemAt(0);
				
				String clip=item.getText().toString();
				//tv.setText(clip);
				//if(tv1.getText().equals(""))
				//{
				//	tv1.setText(clip);
				//}else
				//{tv.setText(clip);}
				if(!clip.contains(" ")||(clip.contains(":")&&clip.length()<25))
				starterup(clip);
				 
				//broadcastIntent(clip);
				
			}
		});

		return START_STICKY;
	}
	public void onDestroy() {
	      super.onDestroy();
	      Toast.makeText(this, "Service Destroyed", Toast.LENGTH_LONG).show();
	   }
	public void broadcastIntent(String value){
    	Intent intent=new Intent();
    	intent.setAction("com.turorialspoint.CUSTOM_INTENT");
    	
    	
    	intent.putExtra("theclip", value);
    	sendBroadcast(intent);
    }
	public void starterup(String value){


Intent dialogIntent = new Intent(getBaseContext(), PopupBible.class);
dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
dialogIntent.putExtra("thisclip", value);
getApplication().startActivity(dialogIntent);


	}

}
