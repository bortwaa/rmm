104.Mark 11:23-24 \The chances of a thing happening should not determine your faith because the magnitude and purity of your faith is what determines what happens
a.Jas 2:26  Until your faith is absolute, it remains dead and unable to produce results
b.Ga 5:6 1Co 13:8\   Faith in the love  of God can never fail to produce results. The love of God is what makes faith a fail-proof force
c.Ga 5:6 1Co 13:8\   Faith in the love  of God can never fail to produce results. The love of God is what makes faith a fail-proof force
James 1:6-8 