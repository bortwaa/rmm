package com.gloryproject.renewingmymind;

import java.io.File;
import java.io.IOException;

import com.gloryproject.renewingmymind.R;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.Intent;
import android.content.res.AssetManager;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

public class Biblelookup extends Activity {
EditText et,et1;Spinner book;
allocation allot=new allocation();

	@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.biblelookup);
	Button b=(Button) findViewById(R.id.button1);
	final Spinner sp=(Spinner) findViewById(R.id.spinner1);
	et=(EditText) findViewById(R.id.editText1);
	et1=(EditText) findViewById(R.id.editText2);
	Uri data = getIntent().getData();
	if(data!=null){
		onBackPressed();
	}
	b.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			int chapter=0;
			int verse=0;
			if((et.getText().toString().length())>0)
			{chapter=Integer.parseInt(et.getText().toString());}
			
            book=(Spinner) findViewById(R.id.spinner1);
			
			
			int booknumber=book.getSelectedItemPosition();
			
			
			int matchbooknumber=allocation.NoChapterbooks[booknumber];
			
			
				
				
		
			
			
			if(et.getText().toString().trim().length()<1&et1.getText().toString().trim().length()<1)
			{
				Toast.makeText(Biblelookup.this, "please enter a chapter and a verse", 5000).show();
			}else
				if(et.getText().toString().length()<1)
				{
					Toast.makeText(Biblelookup.this, "please enter a chapter", 5000).show();
				}else
					
						if(chapter<=matchbooknumber)
			{
							if(et1.getText().toString().length()<1)
							{
								et1.setText("1");
							}			
									Intent i=new Intent(Biblelookup.this,Bible.class);
		
	        int position=sp.getSelectedItemPosition();
			i.putExtra("BOOK", sp.getSelectedItem().toString()
					   +"."+et.getText()+":"+et1.getText()
					   );
			i.putExtra("chapter", Integer.parseInt(et1.getText().toString()));
			i.putExtra("BOOKPOSITION", position);
			et.setText("");
			et1.setText("");
			startActivity(i);
			
			}else
			{
				Toast.makeText(Biblelookup.this, sp.getSelectedItem().toString()+" has only"+" "+matchbooknumber+" chapters" , 5000).show();
			}

	};

	
});
}
	@Override
		protected void onPause() {
			super.onPause();
			
		}
	};
