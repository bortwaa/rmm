39a)Joh 14:6 Jesus saith unto him, I am the way, the truth, and the life: no man cometh unto the Father, but by me. \Jesus is not rude, yet He said it Himself. He did not say He is a way but the way.
39b)Joh 10:9 I am the door: by me if any man enter in, he shall be saved, and shall go in and out, and find pasture
39c)Eph 2:18 For through him we both have access by one Spirit unto the Father.
39d)Heb 10:19 Having therefore, brethren, boldness to enter into the holiest by the blood of Jesus, 20 By a new and living way, which he hath consecrated for us, through the veil, that is to say, his flesh. \Just as life is found in the blood, we have access to the life of God only through the blood of His son because only His son also carries the life of His Father-God.
39e)Heb 9:8  The Holy Ghost this signifying, that the way into the holiest of all was not yet made manifest, while as the first tabernacle was yet standing: \Until the coming of Jesus the Holiest-God's abode, was inaccessible.
39f)Ro 5:19 For as by one man's disobedience many were made sinners, so by the obedience of one shall many be made righteous. \The same principle through which we were separated from God must be the same through which we will be reunited with God. If death came through one man, then life must also come through one man. This satisfies the justice of God.
39g)Ac 4:12 Neither is there salvation in any other: for there is none other name under heaven given among men, whereby we must be saved.
39h)1Ti 2:5 For there is one God, and one mediator between God and men, the man Christ Jesus;
39i)Heb 9:15  And for this cause he is the mediator of the new testament, that by means of death, for the redemption of the transgressions that were under the first testament, they which are called might receive the promise of eternal inheritance.
39j)Heb 12:24 And to Jesus the mediator of the new covenant, and to the blood of sprinkling, that speaketh better things than that of Abel. \The blood of Christ is better than any other way of salvation proclaimed or found in men.
39k)Heb 8:6  But now hath he obtained a more excellent ministry, by how much also he is the mediator of a better covenant, which was established upon better promises.
39l)Heb 7:22 By so much was Jesus made a surety of a better testament.
39m)Pr 14:12  There is a way which seemeth right unto a man, but the end thereof are the ways of death. \Your beliefs, mind and emotions cannot change the truth about the way of God
39n)Heb 5:9 And being made perfect, he became the author of eternal salvation unto all them that obey him;...\He is the perfect way. Obey Him by believing and receiving Him as the only way. 
 