package com.gloryproject.renewingmymind;

import com.parse.ParseException;
import com.parse.ParseObject;
import com.parse.ParseUser;
import com.parse.RequestPasswordResetCallback;
import com.parse.SignUpCallback;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.Preference;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.parse.Parse;
import com.parse.ParseAnalytics;
public class SIGNIN extends Activity implements OnClickListener {
	EditText tvusername,tvpassword;
	TextView forgotpassword;
	Button submit,signup,explore;
	CheckBox chbox;
	
	   public static final String SIGNINDETAILS = "SIGNINDETAILS" ;
	   public static final String username = "user" ;
	   public static final String password = "pass"; 
	   public static final String signedin="signedin";
	   SharedPreferences sharedpreferences,
	   sharedpreferenceskeepsigned;
@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
    Parse.initialize(this, "e5sayD2kSIpaEuB5lqmIjS7sX9v6uihOVBAkUAhN", "Prg8islTdiOwbjmdC1G1hIynRss6RgsVUjk84M1S");

	setContentView(R.layout.signin);

	submit=(Button) findViewById(R.id.submitme);
	signup=(Button) findViewById(R.id.signup);
	signup.setOnClickListener(this);
	tvusername=(EditText) findViewById(R.id.username);
	tvpassword=(EditText) findViewById(R.id.password);
	forgotpassword=(TextView) findViewById(R.id.forgotpassword);
	forgotpassword.setOnClickListener(this);
	explore=(Button) findViewById(R.id.explore);
	explore.setOnClickListener(this);
	
	//chbox=(CheckBox) findViewById(R.id.checkmein);
	
	
	
	
	
    sharedpreferences = getSharedPreferences(SIGNINDETAILS, Context.MODE_PRIVATE);
   
    Editor editor = sharedpreferences.edit();
	editor.putBoolean(signedin, false);
	editor.commit();
	
    if (sharedpreferences.contains(username))
    {
       tvusername.setText(sharedpreferences.getString(username, ""));

    }
    if (sharedpreferences.contains(password))
    {
       tvpassword.setText(sharedpreferences.getString(password, ""));

    }	
   
    
}
public void run(View view){
	
    String n  = tvusername.getText().toString();
    String p  = tvpassword.getText().toString();
    
    if(n.trim().length()<1||p.length()<1){
    	Toast.makeText(getApplicationContext(), "Please fill in the spaces", 5000).show();
    	
    }
    if(sharedpreferences.getString(password, "").trim()!=n.trim()){
    	Toast.makeText(getApplicationContext(), "Wrong Password", 5000).show();
	
    }else
    {
    	if(sharedpreferences.getString(username, "")==n){
    		if(sharedpreferences.getString(password, "")==p){
    			Editor editor = sharedpreferences.edit();
    	    	editor.putBoolean(signedin, true);
    	    	editor.commit();
    	    	
    	    	Intent intent=new Intent(SIGNIN.this,MENU.class);
    	    	startActivity(intent);	
    		}
    	}
    	
    	
    }

     

 }
@Override
public void onClick(View arg0) {
	Dialog signupdialog=new Dialog(SIGNIN.this);
	signupdialog.setContentView(R.layout.signup);
	
	Button signupsubmit=(Button) findViewById(R.id.signupsubmit);
	signupsubmit.setOnClickListener(this);
	if(arg0.getId()==R.id.forgotpassword){
		Dialog d=new Dialog(SIGNIN.this);
		d.setContentView(R.layout.recoveryemail);
		d.show();
		Button b=(Button)d.findViewById(R.id.resetpassword);
		EditText et=(EditText) d.findViewById(R.id.recoveryemail);
		
		ParseUser.requestPasswordResetInBackground(et.getText().toString().trim(),
                new RequestPasswordResetCallback() {
public void done(ParseException e) {
if (e == null) {
// An email was successfully sent with reset instructions.
	Toast.makeText(SIGNIN.this, "An email was successfully sent with reset instructions", 5000).show();
} else {
// Something went wrong. Look at the ParseException to see what's up.
	Toast.makeText(SIGNIN.this, "Something went wrong, please make sure the email is valid and you have internet access", 5000).show();

}
}
});
	}else
	if(arg0.getId()==R.id.explore){
		Intent intent=new Intent(SIGNIN.this,MENU.class);
		Editor editor = sharedpreferences.edit();
    	editor.putBoolean(signedin, false);
    	editor.commit();
		startActivity(intent);
	}else
	if(arg0.getId()==R.id.signup){
		signupdialog.show();
	}
	if(arg0.getId()==R.id.signupsubmit){
		EditText username=(EditText)signupdialog.findViewById(R.id.usernamesignup);
		EditText email=(EditText)signupdialog.findViewById(R.id.signupemail);

		EditText password=(EditText)signupdialog.findViewById(R.id.signuppassword);
		EditText retypepassword=(EditText)signupdialog.findViewById(R.id.signupretypepassword);
		
		if(!password.equals(retypepassword)){
			Toast.makeText(getApplicationContext(), "retype password", 5000).show();
			
		}else{
			ParseUser user = new ParseUser();
			user.setUsername(username.getText().toString());
			user.setPassword(password.getText().toString());
			user.setEmail(email.getText().toString());
			
			// other fields can be set just like with ParseObject
			//user.put("phone", "650-253-0000");
			 
			user.signUpInBackground(new SignUpCallback() {
			  public void done(ParseException e) {
			    if (e == null) {
			      // Hooray! Let them use the app now.
			    	Toast.makeText(getApplicationContext(), "Sign Up was succcessful", 5000).show();
			    	Intent intent=new Intent(SIGNIN.this,QUERY.class);
			    	startActivity(intent);
			    } else {
			      // Sign up didn't succeed. Look at the ParseException
			      // to figure out what went wrong
			    	Toast.makeText(getApplicationContext(), "Sign Up FAILED, Please try again", 5000).show();

			    }
			  }
			});
		}
	}
}
}
