package com.gloryproject.renewingmymind;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import com.gloryproject.renewingmymind.R;
import com.parse.ParseObject;

import android.R.layout;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ListActivity;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Color;
import android.os.Bundle;
import android.text.InputType;
import android.text.Layout;
import android.text.Selection;
import android.text.Spannable;
import android.text.method.MovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.util.Log;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

public class second extends ListActivity {
	///TextView tv,tv1;
	//EditText et;
	//Button b;
	public static int number;
	public String thought,tag,title;
	int thecounter=200;
	int thiscounter=0;
	private String globaltag;
	private List<String>suggestionverses;
	
@Override
protected void onCreate(Bundle bundle) {
	// TODO Auto-generated method stub
	super.onCreate(bundle);
	//setContentView(R.layout.second);
	///tv=(TextView) findViewById(R.id.textView1);
	///tv1=(TextView) findViewById(R.id.textView2);
	//et=(EditText) findViewById(R.id.editText1);
ParseObject object=new ParseObject("testingmic");
object.put("tester", "this is it");
object.saveInBackground();
	
	///Button b=(Button) findViewById(R.id.button);
	
	//tv.setMovementMethod(new ScrollingMovementMethod());
	thought = getIntent().getStringExtra("THOUGHTS");
	tag=getIntent().getStringExtra("thoughttag");
	title=getIntent().getStringExtra("title");
	String allmythoughts=getIntent().getStringExtra("ALLTHOUGHTS");
	String allmycomments=getIntent().getStringExtra("ALLCOMMENTS");
	String SEARCH=getIntent().getStringExtra("SEARCH");
	String mycomments=getIntent().getStringExtra("COMMENT");
	//if(title!=null){tv1.setText(title);}
	if(mycomments!=null){
		String thecomments[] = mycomments.split("\n");
		setListAdapter(new ArrayAdapter<String>(second.this, android.R.layout.simple_list_item_1, thecomments));
	}else
	if(SEARCH!=null){
		if(SEARCH.contains("searchtopics")){searchtopics();}
		else
		{
			
			searchthoughts();
			
		}
	}else
	if (thought!=null) {
		String thethought[] = thought.split("\n");
		setListAdapter(new ArrayAdapter<String>(second.this, android.R.layout.simple_list_item_1, thethought));
	}else
	if(tag!=null){
		
		String thetag[]=tag.split("\n");
	setListAdapter(new ArrayAdapter<String>(second.this, android.R.layout.simple_list_item_1, thetag));
	}else
	if(allmythoughts!=null)	
	{
	try {
		String allthoughts[]=readTHOUGHTSFromAssets("THOUGHTS.txt").split("\n");
		setListAdapter(new ArrayAdapter<String>(second.this, android.R.layout.simple_list_item_1, allthoughts));
	} catch (IOException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	}
	else
	if(allmycomments!=null){
		try {
			String allcomments[]=readAllCOMMENTS().split("\n");
			setListAdapter(new ArrayAdapter<String>(second.this, android.R.layout.simple_list_item_1, allcomments));

		} catch (IOException e) {
			e.printStackTrace();
		}
	}
	
	
	
}
public void searchtopics(){
	AlertDialog.Builder builder=new AlertDialog.Builder(second.this);
	
	{builder.setTitle("SEARCH WORDS IN TAGS");}
	
	final EditText input=new EditText(second.this);
	input.setHint("type search key");
	
	input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
	builder.setView(input);
	
	builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
							
			try {
				
				{
					
					String searched[]=(searchTAGS("ALLTOPICSS.txt", input.getText().toString()).split("\n"));
					int length=input.getText().toString().length();
					if(searched[0].trim().length()==0){
						try {
							
							length=length/2;
						} catch (Exception e) {
						length=(length+1)/2;
						}
						
						searched=searchTAGS("ALLTOPICSS.txt", input.getText().toString().substring(0,length)).split("\n");
					}
					setListAdapter(new ArrayAdapter<String>(second.this, android.R.layout.simple_list_item_1, searched));
					title="TAGS";
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	});
	builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			dialog.cancel();
			
		}
	});
	builder.show();;
}
public void searchthoughts(){
	final String searchallresults="";
	AlertDialog.Builder builder=new AlertDialog.Builder(second.this);
	
	{builder.setTitle("SEARCH WORDS IN THOUGHTS");}
	
	final EditText input=new EditText(second.this);
	input.setHint("type search key");
	
	input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
	builder.setView(input);
	
	builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
							
			try {
				
				{
					String searched[]=(searchTHOUGHTSFromAssets("THOUGHTS.txt", input.getText().toString()).split("\n"));
					if(searched[0].trim().length()==0){
						int length=input.getText().toString().length();
						try {
							length=length/2;
						} catch (Exception e) {
							length=(length+1)/2;
						}
						searched=searchTHOUGHTSFromAssets("THOUGHTS.txt", input.getText().toString().substring(0,length)).split("\n");
					}
					setListAdapter(new ArrayAdapter<String>(second.this, android.R.layout.simple_list_item_1, searched));

				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	});
	builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			dialog.cancel();
			
		}
	});
	builder.show();;
}
public void searchconcordance() throws IOException{
	//InputStream inputStream1 = this.getResources().getAssets().open("Refined Thes.txt");
	//final BufferedReader reader1 = new BufferedReader(new InputStreamReader(inputStream1));
	AlertDialog.Builder builder=new AlertDialog.Builder(second.this);
	
	{builder.setTitle("SEARCH WORDS IN CONCORDANCE");}
	
	final EditText input=new EditText(second.this);
	input.setHint("type search key");
	
	input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
	builder.setView(input);
	
	builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
							
			try {
				
				{
					String searched[]=(searchTAGS("ALLTOPICS", input.getText().toString()).split("\n"));
					if(searched[0].trim().length()==0){
						int length=input.getText().toString().length();
						try {
							length=length/2;
						} catch (Exception e) {
							length=(length+1)/2;
						}
						searched=searchTAGS("ALLTOPICS", input.getText().toString().substring(0,length)).split("\n");
					}
					
					setListAdapter(new ArrayAdapter<String>(second.this, android.R.layout.simple_list_item_1, searched));
title="TOPICS";
				}

			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	});
	builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			dialog.cancel();
			
		}
	});
	builder.show();;
}
@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		MenuInflater inflater=getMenuInflater();
		inflater.inflate(R.menu.menu, menu);
		return true;
	}
@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		
	if (item.getItemId()==R.id.searchthought) {
		searchthoughts();
		}
	if (item.getItemId()==R.id.searchtags) {
		searchtopics();
		}
	if (item.getItemId()==R.id.searchconcordance) {
		try {
			searchconcordance();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	if(item.getItemId()==R.id.mainmenu){
		Intent i=new Intent(second.this,MENU.class);
		startActivity(i);
	}
		return super.onOptionsItemSelected(item);
	}
public String readTHOUGHTSFromAssets(String filename) throws IOException{
	InputStream inputStream = this.getResources().getAssets().open(filename);
	BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	String line="";
    String expectedLine=""; 
        while ((line = reader.readLine()) != null) {
          expectedLine=expectedLine+line+"\n";
          
        }
		return expectedLine;
}
public int getCurrentLine(EditText editText)
{
int selectionStart=Selection.getSelectionStart(editText.getText());
Layout layout=editText.getLayout();
if (!(selectionStart==-1)) {
	return layout.getLineForOffset(selectionStart);
}
return -1;
}
public String searchTHOUGHTSFromAssets(String filename,String search) throws IOException{
	InputStream inputStream = this.getResources().getAssets().open(filename);
	BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	
	String line="";
	String searchkey=search.toUpperCase();
    String expectedLine=""; 
        while ((line = reader.readLine()) != null) {
        	if(line.toUpperCase().contains(searchkey.trim())){
        		 expectedLine=expectedLine+line+"\n";
            }
         
          
        }
		return expectedLine;
}
public String searchTAGS(String filename,String search) throws IOException{
	InputStream inputStream = this.getResources().getAssets().open(filename);
	BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	
	//InputStream inputStream1 = this.getResources().getAssets().open("Refined Thes.txt");
	//BufferedReader reader1 = new BufferedReader(new InputStreamReader(inputStream1));
	
	String line="";
	String searchkey=search.toUpperCase();
    String expectedLine=""; 
        while ((line = reader.readLine()) != null) {
        	if(line.toUpperCase().contains(searchkey.trim())){
        		 expectedLine=expectedLine+line.replaceAll("\\d", "")+"\n";
            }
         
          
        }
        
		return expectedLine;
}
public void alert(){
	AlertDialog.Builder builder=new AlertDialog.Builder(second.this);
	
	builder.setTitle("Please type in the thought number");
	
	final EditText input=new EditText(second.this);
	input.setHint("numbers only");
	input.setInputType(number);
	
	input.setInputType(InputType.TYPE_CLASS_TEXT | InputType.TYPE_TEXT_VARIATION_NORMAL);
	builder.setView(input);
	
	builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			
			
			
		}
	});
	builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
		
		@Override
		public void onClick(DialogInterface dialog, int which) {
			dialog.cancel();
			
		}
	});
	builder.show();
}
public String readTHOUGHTTAGSFromAssets(String tagfile, String tag)
		throws IOException {
	InputStream inputStream = this.getResources().getAssets().open(tagfile);

	BufferedReader reader = new BufferedReader(new InputStreamReader(
			inputStream));

	String line = "";
globaltag=tag;
	int lineCounter = 0;
int counter=0;
	String expectedLine = "";

	while ((line = reader.readLine()) != null) {
		lineCounter++;
		
		counter++;
		
		
		if (line.toLowerCase().contains(tag.trim().toLowerCase())) {
			expectedLine = expectedLine
					+ selectTHOUGHTFromAssets("THOUGHTS.txt", lineCounter)
					+ "\n";
			
		}
		
		

	}
	return expectedLine;
}
public String selectTHOUGHTFromAssets(String filename, int number)
		throws IOException {
	InputStream inputStream = this.getResources().getAssets()
			.open(filename);
	BufferedReader reader = new BufferedReader(new InputStreamReader(
			inputStream));

	String line = null;

	int lineCounter = 0;

	String expectedLine = null;

	while ((line = reader.readLine()) != null) {
		lineCounter++;
		if (lineCounter == number) {

			expectedLine = line;
		}

	}
	return expectedLine;
}
public String readAllCOMMENTS() throws IOException{
	String expectedLine=""; 
    for(int i=1;i<223;i++){
        expectedLine=expectedLine+"\n"+readAllCOMMENTSSFromResponseFile(String.valueOf(i)+".txt");         
      
    }
		return expectedLine;
}
public String readAllCOMMENTSSFromResponseFile(String filename) throws IOException{
	InputStream inputStream = this.getResources().getAssets().open(filename);
	BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	String line="";
    String expectedLine=""; 
        while ((line = reader.readLine()) != null) {
        	if(line.contains("\\"))
          {expectedLine=expectedLine+line.substring(line.indexOf("\\")+1)+"\n";}
          
        }
		return expectedLine;
}

public String readAllVERSESFromAssets(String filename) throws IOException{
	InputStream inputStream = this.getResources().getAssets().open(filename);
	BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));

	String line = "";

	
    String expectedLine=""; 
        while ((line = reader.readLine()) != null) {
        	if(line.length()<1){line="";}
        	else
        	if(line.contains("\\")){
        		
        		if(line.substring(0, line.indexOf("\\")).length()<15){
        line=readBible(line.substring(0,line.indexOf("\\")))+"\n"+line.substring(line.indexOf("\\")+1);
        		}
        		
        	}else
        		if(line.length()<15){
        			line=readBible(line);
        		}
        	
          expectedLine=expectedLine+line+"\n";
          
        }
		

		return expectedLine;
		
}
@Override
protected void onListItemClick(ListView l, View v, int position, long id) {
	super.onListItemClick(l, v, position, id);
	String record=l.getItemAtPosition(position).toString();
	String filename=null;
	if(record.contains(".")){
	filename=record.trim().substring(0, record.trim().indexOf("."))+".txt";
	}
		Dialog d=new Dialog(second.this);
		d.setContentView(R.layout.dialogdisplay);
		d.setTitle(record);
		
		final TextView verseview=(TextView)d.findViewById(R.id.versedisplay);
		
		Dialog myd=new Dialog(second.this);
		myd.setContentView(R.layout.suggestionbox);
		myd.setTitle(record);
		
		final TextView suggestview=(TextView) myd.findViewById(R.id.suggestions);
		final Button suggest=(Button) myd.findViewById(R.id.NextSuggestions);

		verseview.setMovementMethod(new ScrollingMovementMethod());
		if(title.contains("TAGS")&filename==null){
			{ 
				if(record.contains("CLICK HERE FOR MORE THOUGHTS")){
					try {
						readTHOUGHTTAGSFromAssets("TAGS.txt", globaltag);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
				}else{
				try {
					thiscounter=0;
					String pattern[]=readTHOUGHTTAGSFromAssets("TAGS.txt", record.trim()).split("\n");
					
					setListAdapter(new ArrayAdapter<String>(second.this, android.R.layout.simple_list_item_1,pattern));
				} catch (IOException e1) {
					// TODO Auto-generated catch block
					e1.printStackTrace();
				}
				}
		}
		}else
if(filename==null){
	
	if(title.contains("TOPICS")){
		if(record.contains("CLICK HERE FOR MORE")){
			try {
				String[] more=(ALLTOPICS()).split("\n");
				setListAdapter(new ArrayAdapter<String>(second.this, android.R.layout.simple_list_item_1,more));
				Toast.makeText(second.this, "this is it", 5000).show();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		
		else{
		try {
			verseview.setText(showconcordanceverses(record));
			Toast.makeText(this, "concordance", 5000).show();
			d.show();
			final MainActivity main=new MainActivity();
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		}
	}
	else
		
	{
	try {
		String pattern[]=readTHOUGHTTAGSFromAssets("TAGS.txt", record.trim()).split("\n");
		setListAdapter(new ArrayAdapter<String>(second.this, android.R.layout.simple_list_item_1,pattern));
	} catch (IOException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
}

}
else
	if(filename!=null)
{
try {
	verseview.setText(record+"\n"+"\n"+readAllVERSESFromAssets(filename));
} catch (IOException e) {
	
	e.printStackTrace();
}

		//title.setText(record);
		//verseview.setText("this is it");
		d.show();
	
	Toast.makeText(second.this, record, 5000).show();
}
}

public  String readBible(String expectedLine) throws IOException {
	if(expectedLine.contains(",")){expectedLine=expectedLine.replaceAll(",", "");}

	if(expectedLine.contains(".")){
	expectedLine=expectedLine.substring(expectedLine.indexOf(".")+1);
	}
	if(expectedLine.contains(")")){
		expectedLine=expectedLine.substring(expectedLine.indexOf(")")+1);
		}
	allocation allot=new allocation();
	String bibleverse=allot.books(expectedLine);
	String fullverse="";
	if(bibleverse.contains("-")){
		fullverse=bible(bibleverse.substring(0,bibleverse.indexOf(".")), 
				bibleverse.substring(bibleverse.indexOf(".")+1,bibleverse.indexOf(":")), 
				bibleverse.substring(bibleverse.indexOf(":")+1,bibleverse.indexOf("-")),
				bibleverse.substring(bibleverse.indexOf("-")+1));	
	}else{
		fullverse=bible(bibleverse.substring(0,bibleverse.indexOf(".")), 
				bibleverse.substring(bibleverse.indexOf(".")+1,bibleverse.indexOf(":")), 
				bibleverse.substring(bibleverse.indexOf(":")+1),
				"");
	}
	return fullverse;
}
public  String bible(String BOOK,String CHAPTER,String VERSE,String END) throws IOException{
	InputStream inputStream = this.getResources().getAssets().open(BOOK+".txt");
	BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	
	int linecount=0;
	String expectedLine="";
	String line="";
	int counter=0;
	int limiter=0;
	boolean begin=false;
	 boolean endverse=false;
if(!END.equals("")){limiter=Integer.parseInt(END)-Integer.parseInt(VERSE);}
	 while ((line = reader.readLine()) != null) {
     	linecount++;
       if(BOOK.toLowerCase().contains("psal")){
     	  
     	  if(line.contains("PSALM "+CHAPTER))
 {   
     		  begin=true;
 }
     		  if(begin==true){
     		  if(line.trim().startsWith(VERSE+" ")){
     			  
	 expectedLine=BOOK+CHAPTER+":"+line;break;
	 
	 
     		  }
 }

     		  
     	      }  
       else{
       if(line.contains("CHAPTER "+CHAPTER))
       {   
    	 begin=true;  
    	  //expectedLine=line;break; 
       
     }
      
       if(begin==true){
       if(line.trim().startsWith(VERSE+" ")||endverse==true)
       {
    	   if(END.equals(""))
    	   {expectedLine=BOOK+CHAPTER+":"+line;break;}
    	   else
    	   {
    		   endverse=true;
    		   expectedLine=expectedLine+line;
    		  counter++;
    		  if(counter==limiter+1){
    			  expectedLine=BOOK+CHAPTER+":"+expectedLine;
    			  begin=false;break;
    		  }
    	   }
    		   
    	   
       }
       
       }
	 }
}
	 return expectedLine;	 
}
public String showconcordanceverses(String word) throws IOException {
	InputStream inputStream = this.getResources().getAssets().open(word.trim().substring(0,1)+".txt");
	BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	String line = null;
	int counter=0;
	boolean begin=false;
	String expectedLine = "";
	//suggestionverses.removeAll(suggestionverses);
	while ((line = reader.readLine()) != null) {
		
		if(line.toLowerCase().trim()==(word.trim().toLowerCase())){
			begin=true;
			
		}
		if(begin==true){
			if(line.contains(":")){
			//suggestionverses.add(line);
		expectedLine = expectedLine + readBible(line) + "\n"+"\n";
		counter++;
			}
			if(counter>1&!line.contains(":")){begin=false;break;}
		}
		
	}
	return expectedLine;
}
public String ALLTOPICS() throws IOException {
	InputStream inputStream2 = this.getResources().getAssets()
			.open("ALLTOPICS");
	BufferedReader reader2 = new BufferedReader(new InputStreamReader(
			inputStream2));

	String line = null;
	String expectedLine = "";
	int counter = 0;
	int counter1 = 0;
	int number = 1;
	while ((line = reader2.readLine()) != null) {
		
		counter++;
		if(counter>thecounter-1){
			expectedLine=expectedLine+line+"\n";
			
			if(thecounter<counter-thecounter-1){
			expectedLine=expectedLine+"CLICK HERE FOR MORE";
			thecounter=counter;
			break;
			}
		}
	}

	return expectedLine;

}

@Override
protected void onPause() {
	super.onPause();
	finish();
	//System.exit(0);

}
}
