package com.gloryproject.renewingmymind;

import java.util.Map;
import java.util.Set;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.content.SharedPreferences.OnSharedPreferenceChangeListener;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RatingBar;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.RatingBar.OnRatingBarChangeListener;

public class QUERY extends Activity implements OnClickListener{
	public String groupone="(fun-loving extrovert; outgoing; very social; 'the life of the party') - EXTROVERT";
	public String grouptwo="(focused; extrovert; goal oriented; 'the achiever') - EXTROVERT";
	public String groupthree="(detailed; introspective; artistic; 'the naturally gifted') - INTROVERT";
	public String groupfour="(easy going; stable; consistent; 'the loyal friend') - INTROVERT";

	public String firstrating;
	public String secondrating;
	public String thirdrating;
	public String fourthrating;
	
	public String userdetails;

	Button submitqueryresults,next;
	TextView temperamentprofile;
	Spinner sections,rater;
	SharedPreferences sharedpreferences,sharedpreferences1;
@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.query);
	temperamentprofile=(TextView) findViewById(R.id.temperatmentprofile);
	sections=(Spinner) findViewById(R.id.sections);
	rater=(Spinner) findViewById(R.id.rater);
	
	submitqueryresults=(Button) findViewById(R.id.submitqueryresults);
	next=(Button) findViewById(R.id.NEXT);
	
	submitqueryresults.setOnClickListener(this);
	next.setOnClickListener(this);
	
	temperamentprofile.setText(groupone);
}
@Override
public void onClick(View v) {
	
		
	int position=sections.getSelectedItemPosition();
	
	if(position>4){
		position=0;
	}
	if(v.getId()==R.id.submitqueryresults){
		Intent intent=new Intent(QUERY.this,MENU.class);
		//intent.putExtra("userdetails", userdetails);
	      sharedpreferences = getSharedPreferences("SUBMITQUERIES", Context.MODE_PRIVATE);	
	     sharedpreferences1 = getSharedPreferences("SIGNINDETAILS", Context.MODE_PRIVATE);	

	      String temperamentprofile=sharedpreferences.getString("username", "");
	      //Editor editor = sharedpreferences.edit();
	     // editor.putString("USERDETAILS", userdetails);
		
	      Editor editor = sharedpreferences.edit();
	    	editor.putString("temperamentprofile", userdetails);
	    	editor.commit();
	    Editor editor1=sharedpreferences1.edit();
	    editor1.putBoolean("signedin", true);
	    		
		
		
		startActivity(intent);
	}
   if(v.getId()==R.id.NEXT){
	   if(position==0)
	   {
		   firstrating=sections.getSelectedItem().toString()+
		   rater.getSelectedItem().toString().substring(0,2)+".";  
			temperamentprofile.setText(grouptwo);

	   }
	   if(position==1)
	   {
		   secondrating=sections.getSelectedItem().toString()+
		   rater.getSelectedItem().toString().substring(0,2)+"."; 
			temperamentprofile.setText(groupthree);

	   }
	   if(position==2)
	   {
		   thirdrating=sections.getSelectedItem().toString()+
		   rater.getSelectedItem().toString().substring(0,2)+".";  
			temperamentprofile.setText(groupfour);

	   }
	   if(position==3)
	   {
		   fourthrating=sections.getSelectedItem().toString()+
		   rater.getSelectedItem().toString().substring(0,2);  
		   
			temperamentprofile.setText(groupone);

	   }
	   userdetails=firstrating+secondrating+thirdrating
			   +fourthrating;
	   sections.setSelection(position);
	   position++;
	}
   if(v.getId()==R.id.rater){
		
	}
	
}

}