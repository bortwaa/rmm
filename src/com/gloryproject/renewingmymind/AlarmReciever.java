package com.gloryproject.renewingmymind;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.app.TaskStackBuilder;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NotificationCompat;
import android.widget.Toast;

public class AlarmReciever extends BroadcastReceiver {

	@Override
	public void onReceive(Context context, Intent intent) {
		try {
		     Bundle bundle = intent.getExtras();
		     String message = bundle.getString("alarm_message");
		     Toast.makeText(context, message, Toast.LENGTH_SHORT).show();
		    } catch (Exception e) {
		     Toast.makeText(context, "There was an error somewhere, but we still received an alarm", Toast.LENGTH_SHORT).show();
		     e.printStackTrace();
		 
		    }
		NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(context);
		mBuilder.setSmallIcon(R.drawable.ic_launcher);
		mBuilder.setContentTitle("Notification Alert, Click Me!");
		mBuilder.setContentText("Hi, Time to renew your mind");
		
		
		Intent resultIntent = new Intent(context, SPLASHSCREEN.class);
		
		PendingIntent resultPendingIntent =
			    PendingIntent.getActivity(context, 0, resultIntent, PendingIntent.FLAG_UPDATE_CURRENT
			);	
		mBuilder.setContentIntent(resultPendingIntent);
		
		
		NotificationManager mNotificationManager =
			    (NotificationManager)context. getSystemService(Context.NOTIFICATION_SERVICE);
			    
			int notificationID = 0;
			// notificationID allows you to update the notification later on.
			mNotificationManager.notify(notificationID, mBuilder.build());
	}

}
