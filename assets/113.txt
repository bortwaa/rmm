113a)Mt 18:22 Jesus saith unto him, I say not unto thee, Until seven times: but, Until seventy times seven.
113b)Mt 6:15 But if ye forgive not men their trespasses, neither will your Father forgive your trespasses.
113c)Mt 18:35 So likewise shall my heavenly Father do also unto you, if ye from your hearts forgive not every one his brother their trespasses. \If I don't get tired of forgiving, God will not get tired of forgiving me.
113d)Jas 2:13 For he shall have judgment without mercy, that hath shewed no mercy; and mercy rejoiceth against judgment.
113e)Lu 6:38... For with the same measure that ye mete withal it shall be measured to you again. 
113f)1Pe 4:8 And above all things have fervent charity among yourselves: for charity shall cover the multitude of sins.
113g)1Co 13:8  Charity never faileth:...   \Love never fails to forgive no matter how many times it forgives. 
113h)Mt 5:7 Blessed are the merciful: for they shall obtain mercy.
113i)Ho 6:6 For I desired mercy, and not sacrifice; and the knowledge of God more than burnt offerings.
113j)Heb 4:16 Let us therefore come boldly unto the throne of grace, that we may obtain mercy, and find grace to help in time of need.  \God deals with us on the platform of mercy so that we will deal with all men on the platform of mercy.

