package com.gloryproject.renewingmymind;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.gloryproject.renewingmymind.R;
import com.gloryproject.renewingmymind.MainActivity;
import com.gloryproject.renewingmymind.second;
import com.parse.Parse;
import com.parse.ParseObject;

import android.R.color;
import android.os.Bundle;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.SQLException;
import android.support.v7.app.ActionBarActivity;
import android.text.Html;
import android.text.InputType;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.method.ScrollingMovementMethod;
import android.text.style.URLSpan;
import android.text.util.Linkify;
import android.util.Log;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ContextMenu.ContextMenuInfo;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MainActivity extends Activity implements OnClickListener {
	private TextView thoughtview, verseview, commentview, topicalview,
			tagsview;
	private Button generate, b1;
	public int linenumber = 1;
	public int lineCounter = 1;
	public int lastline = 1;
	public int setter = 0;
	public int linemarker = 0;

	private int topical = 1;
	private String THOUGHTS = "THOUGHTS";
	private String COMMENTS = "COMMENTS";
	private String VERSES = "VERSES";
	private String TOPICS = "TOPICS";
	private String DEVOTIONALS = "DEVOTIONALS";
	private String temperament = "";
	String selected;
	protected boolean showreferences;
	private int start = 0;

	public boolean username;

	SharedPreferences sharedpreferencesuser, sharedPreferencestemperament;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Parse.initialize(this, "e5sayD2kSIpaEuB5lqmIjS7sX9v6uihOVBAkUAhN",
				"Prg8islTdiOwbjmdC1G1hIynRss6RgsVUjk84M1S");

		setContentView(R.layout.activity_main);

		sharedpreferencesuser = getSharedPreferences("SIGNINDETAILS",
				Context.MODE_PRIVATE);
		sharedPreferencestemperament = getSharedPreferences("SUBMITQUERIES",
				Context.MODE_PRIVATE);
		if (sharedpreferencesuser.contains("username")) {
			username = sharedpreferencesuser.getBoolean("signedin", false);

		}
		if (sharedPreferencestemperament.contains("temperamentprofile")) {
			temperament = sharedPreferencestemperament.getString(
					"temperamentprofile", "");

		}

		thoughtview = (TextView) findViewById(R.id.textView1);
		registerForContextMenu(thoughtview);
		verseview = (TextView) findViewById(R.id.textView2);
		verseview.setMovementMethod(new ScrollingMovementMethod());
		commentview = (TextView) findViewById(R.id.textView3);
		commentview.setMovementMethod(new ScrollingMovementMethod());
		topicalview = (TextView) findViewById(R.id.textView12);
		registerForContextMenu(topicalview);
		tagsview = (TextView) findViewById(R.id.textView4);
		registerForContextMenu(tagsview);
		// tagsview.setMovementMethod(LinkMovementMethod.getInstance());

		// TextView views[]={t1,t2};
		RelativeLayout mylayout = (RelativeLayout) findViewById(R.id.Rlayout);
		mylayout.getBackground().setAlpha(50);

		generate = (Button) findViewById(R.id.button2);
		registerForContextMenu(generate);
		b1 = (Button) findViewById(R.id.button1);
		registerForContextMenu(b1);
		Button b2 = (Button) findViewById(R.id.button3);
		// tdatasource=new Thoughtsdatasource(this);
		// tdatasource.open();

		String key = getIntent().getStringExtra("MODE");
		if (key != null) {
			b1.setText("COMMENTS");
		}

		String number = getIntent().getStringExtra("NUMBER");

		String title = getIntent().getStringExtra("TITLE");

		String thetag = getIntent().getStringExtra("NUMBERTAG");
		if (thetag == null) {
			thetag = "";
		}
		if (thetag.contains("1") || thetag.contains("2")
				|| thetag.contains("3") || thetag.contains("4")
				|| thetag.contains("5") || thetag.contains("6")
				|| thetag.contains("7") || thetag.contains("8")
				|| thetag.contains("9")) {
			topicalview.setText(thetag.replaceAll("\\d", ""));
		}
		// tv4.setText(thetag);
		Toast.makeText(this, number + thetag + title, 5000).show();
		thoughtview.setHint("CLICK THE 'BIG' BUTTON TO GENERATE THOUGHTS"
				+ "\n" + "LONG CLICK ON ANY BUTTON FOR MORE OPTIONS");
		if (topicalview.getText().toString().length() == 0) {
			topicalview.setText("RANDOM RECORDS");
		}

		try {
			thoughtview.setText(selectTHOUGHTFromAssets("THOUGHTS.txt",
					Integer.parseInt(number)));
			if (number != null) {
				linenumber = Integer.parseInt(number);
			}
			readTAGSFromAssets("TAGS.txt");

		} catch (NumberFormatException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IOException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}
		String versekey = getIntent().getStringExtra("VERSEKEY");
		if (versekey != null) {
			b1.setText("VERSES");
		}
		topicalview.setText("ALWAYS LONG CLICK HERE TO SELECT A TOPIC");
		// try {
		// GENERATE();
		// } catch (IOException e1) {
		// TODO Auto-generated catch block
		// e1.printStackTrace();
		// }
		b2.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Dialog contributebox = new Dialog(MainActivity.this);
				contributebox.setContentView(R.layout.additions);
				TextView tv = (TextView) contributebox
						.findViewById(R.id.contributetextview);
				tv.setText(thoughtview.getText().toString() + "\n"
						+ verseview.getText().toString() + "\n"
						+ commentview.getText().toString());
				contributebox.show();

				Button addthought = (Button) contributebox
						.findViewById(R.id.addthought);
				Button addverse = (Button) contributebox
						.findViewById(R.id.addverse);
				Button addcomment = (Button) contributebox
						.findViewById(R.id.addcomment);
				Button helpful = (Button) contributebox
						.findViewById(R.id.helpful);
				Button demote = (Button) contributebox
						.findViewById(R.id.demote);
				Button unclear = (Button) contributebox
						.findViewById(R.id.unclear);

				addthought.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						addthought();
					}
				});
				addverse.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						addverse();
					}
				});
				addcomment.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						addcomment();
					}
				});
				helpful.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						helpful();
					}
				});
				demote.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						demote();
					}
				});
				unclear.setOnClickListener(new OnClickListener() {
					@Override
					public void onClick(View arg0) {
						unclear();
					}
				});

			}
		});
		generate.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				try {
					if (topicalview.getText().toString()
							.contains("LONG CLICK HERE TO SELECT A TOPIC")) {
						topicalview.setText("RANDOM RECORDS");
					}
					GENERATE();

				} catch (IOException e) {
					e.printStackTrace();
				}

			}
		});
		b1.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				next();

			}
		});
		tagsview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				if (tagsview.getSelectionStart() == -1
						&& tagsview.getSelectionEnd() == -1) {
					// Toast.makeText(getApplicationContext(),
					// "You clicked outside the link",
					// Toast.LENGTH_SHORT).show();
				} else {

					int start = tagsview.getSelectionStart();
					int end = tagsview.getSelectionEnd();
					selected = tagsview.getText().toString()
							.substring(start, end);

					Intent i = new Intent(MainActivity.this, second.class);

					// tv.setText(readBible(selected));
					// d.show();
					// i.putExtra("VERSE", selected);
					String bundlethoughts = "";
					try {
						bundlethoughts = readTHOUGHTTAGSFromAssets1("TAGS.txt",
								selected);
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

					i.putExtra("THOUGHTS", bundlethoughts);
					i.putExtra("title", selected);
					startActivity(i);
					Toast.makeText(getApplicationContext(), "tags",

					Toast.LENGTH_LONG).show();

				}
			}
		});
		verseview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				Dialog d = new Dialog(MainActivity.this);
				d.setContentView(R.layout.dialogdisplay);
				d.setTitle(thoughtview.getText().toString());

				final TextView allverse = (TextView) d
						.findViewById(R.id.versedisplay);
				allverse.setMovementMethod(new ScrollingMovementMethod());
				allverse.setText(tagsview.getText().toString() + "\n" + "\n"
						+ verseview.getText().toString() + "\n" + "\n"
						+ commentview.getText().toString());
				d.show();
			}
		});
		verseview.setOnLongClickListener(new OnLongClickListener() {

			@Override
			public boolean onLongClick(View arg0) {

				return false;

			}
		});
		thoughtview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

			}
		});
		topicalview.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {

				if (topicalview.getText().toString().toUpperCase()
						.contains("RANDOM")) {
					topicalview.setText("RIGHTEOUSNESS"

					);
				} else if (topicalview.getText().toString().toUpperCase()
						.contains("RIGHTEOUSNESS")) {
					topicalview.setText("SIN-NATURE"

					);
				} else if (topicalview.getText().toString().toUpperCase()
						.contains("SIN-NATURE")) {
					topicalview.setText("SIN"

					);
				} else if (topicalview.getText().toString().toUpperCase()
						.contains("SIN")) {
					topicalview.setText("MIND-TURBULENCE"

					);
				} else if (topicalview.getText().toString().toUpperCase()
						.contains("MIND-TURBULENCE")) {
					topicalview.setText("IDENTITY-IN-CHRIST"

					);
				} else if (topicalview.getText().toString().toUpperCase()
						.contains("IDENTITY-IN-CHRIST")) {
					topicalview.setText("QUALIFICATIONS"

					);

				} else {
					topicalview.setText("RANDOM RECORDS"

					);
				}

			}
		});
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		// getMenuInflater().inflate(R.menu.actionbar, menu);
		return true;
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		if (item.getItemId() == R.id.item1) {
			// Intent i = new Intent(MainActivity.this, second.class);
			// i.putExtra("ALLTHOUGHTS", "YES");
			// startActivity(i);
			Intent i = new Intent(MainActivity.this, second.class);
			Intent i2 = new Intent(MainActivity.this, second.class);
			try {
				i.putExtra("thoughttag", ALLTAGS());
				i2.putExtra("title", "TAGS");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			startActivity(i);

		} else if (item.getItemId() == R.id.action_settings) {
			Intent i = new Intent(MainActivity.this, second.class);
			Intent i2 = new Intent(MainActivity.this, second.class);
			try {
				i.putExtra("thoughttag", ALLTAGS());
				i.putExtra("title", "TAGS");
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			startActivity(i);

		} else if (item.getItemId() == R.id.bible) {
			Intent i = new Intent(MainActivity.this, Biblelookup.class);
			startActivity(i);
		} else if (item.getItemId() == R.id.searchtopics) {
			Intent i = new Intent(MainActivity.this, second.class);
			i.putExtra("SEARCH", "searchtopics");
			i.putExtra("title", "TAGS");
			Toast.makeText(getApplicationContext(), "search from option menu",
					5000).show();

			startActivity(i);
		} else if (item.getItemId() == R.id.searchthoughts) {
			Intent i = new Intent(MainActivity.this, second.class);
			i.putExtra("SEARCH", "searchthoughts");

			Toast.makeText(getApplicationContext(), "search from option menu",
					5000).show();
			startActivity(i);
		}
		if (item.getItemId() == R.id.showcomments) {
			Intent i = new Intent(MainActivity.this, second.class);
			i.putExtra("ALLCOMMENTS", "YES");
			startActivity(i);

		}
		if (item.getItemId() == R.id.themainmenu) {
			Intent i = new Intent(MainActivity.this, MENU.class);
			startActivity(i);
		}
		// if(item.getItemId()==R.id.f){
		if (item.getItemId() == R.id.sharing) {
			String heading = "";
			String verse = "";
			String meditation = "";

			if (b1.getText().toString().contains("THOUGHTS")) {
				heading = "Negative thought";
				meditation = "meditation: ";
			} else if (b1.getText().toString().contains("COMMENTS")) {
				heading = "Quote: ";
				meditation = "";
				thoughtview.append("{");
			} else if (b1.getText().toString().contains("VERSES")) {
				heading = "";
				meditation = "meditation: ";
			}
			// formating text
			String subject = thoughtview.getText().toString();
			String tail = commentview.getText().toString();
			String middle = verseview.getText().toString();
			if (thoughtview.getText().toString().contains("{")) {
				subject = thoughtview
						.getText()
						.toString()
						.substring(0,
								thoughtview.getText().toString().indexOf("{"));
			}
			if (commentview.getText().toString().contains("{")) {
				tail = commentview
						.getText()
						.toString()
						.substring(0,
								commentview.getText().toString().indexOf("{"));
			}

			Intent shareintent = new Intent(Intent.ACTION_SEND);
			shareintent.setType("text/plain");
			shareintent.putExtra(Intent.EXTRA_TEXT, heading + subject + "\n"
					+ middle + "\n" + meditation + "-" + tail);
			shareintent
					.putExtra(
							Intent.EXTRA_SUBJECT,
							"Extract from "
									+ "https://play.google.com/store/apps/details?id=com.gloryproject.renewingmymind"
									+ "\n");
			startActivity(Intent.createChooser(shareintent, "share"));

		}

		return super.onOptionsItemSelected(item);
	}

	public String readTHOUGHTSFromAssets(String filename) throws IOException {
		InputStream inputStream = this.getResources().getAssets()
				.open(filename);
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));

		String line = null;

		int lineCounter = 0;

		String expectedLine = null;
		linenumber = (int) (Math.random() * 1513 + 1);
		while ((line = reader.readLine()) != null) {
			lineCounter++;
			if (lineCounter == linenumber) {

				expectedLine = line;
			}

		}
		return expectedLine;
	}

	public String readVERSESFromAssets(String filename) throws IOException {
		InputStream inputStream = this.getResources().getAssets()
				.open(filename);
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));

		String line = null;

		int lineCount = 0;
		String expectedLine = null;

		while ((line = reader.readLine()) != null) {
			lineCount++;
			if (lineCount == lineCounter) {

				expectedLine = line;
			}

		}
		lastline = lineCount + 1;

		if (expectedLine.length() < 1) {
			expectedLine = "END";
		} else {
			if (expectedLine.length() < 30) {
				expectedLine = readBible(expectedLine);
			}
			if (expectedLine.contains("\\")) {
				if (expectedLine.substring(0, expectedLine.indexOf("\\"))
						.length() < 30) {
					expectedLine = readBible(expectedLine.substring(0,
							expectedLine.indexOf("\\")))
							+ expectedLine
									.substring(expectedLine.indexOf("\\"));
				}
			}
		}
		return expectedLine;
	}

	public String readBible(String expectedLine) throws IOException {
		if (expectedLine.contains(",")) {
			expectedLine = expectedLine.replaceAll(",", "");
		}

		if (expectedLine.contains(".")) {
			expectedLine = expectedLine
					.substring(expectedLine.indexOf(".") + 1);
		}
		if (expectedLine.contains(")")) {
			expectedLine = expectedLine
					.substring(expectedLine.indexOf(")") + 1);
		}
		allocation allot = new allocation();
		String bibleverse = allot.books(expectedLine);
		String fullverse = "";
		if (bibleverse.contains("-")) {
			fullverse = bible(bibleverse.substring(0, bibleverse.indexOf(".")),
					bibleverse.substring(bibleverse.indexOf(".") + 1,
							bibleverse.indexOf(":")), bibleverse.substring(
							bibleverse.indexOf(":") + 1,
							bibleverse.indexOf("-")),
					bibleverse.substring(bibleverse.indexOf("-") + 1));
		} else {
			fullverse = bible(bibleverse.substring(0, bibleverse.indexOf(".")),
					bibleverse.substring(bibleverse.indexOf(".") + 1,
							bibleverse.indexOf(":")),
					bibleverse.substring(bibleverse.indexOf(":") + 1), "");
		}
		return fullverse;
	}

	public String readAVERSESandCOMMENT(String filename) throws IOException {
		InputStream inputStream = this.getResources().getAssets()
				.open(filename);

		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));

		String line = null;

		int lineCount = 0;
		int commentcounter = NoOfComments(filename);
		String expectedLine = null;

		int chosencomment = (int) (Math.random() * commentcounter + 1);
		while ((line = reader.readLine()) != null) {

			if (line.contains("\\")) {
				lineCount++;
				if (chosencomment == lineCount) {
					expectedLine = line;
				}
			}

		}

		if (expectedLine == null) {
			expectedLine = "  \\  ";
		}
		lastline = lineCount + 1;

		return expectedLine;
	}

	public int NoOfComments(String filename) throws IOException {
		InputStream inputStream = this.getResources().getAssets()
				.open(filename);

		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));

		String line = null;

		int lineCount = 0;
		int commentcounter = 0;
		String expectedLine = null;

		while ((line = reader.readLine()) != null) {
			lineCount++;
			if (line.contains("\\")) {
				commentcounter++;

			}

		}
		return commentcounter;
	}

	public String readAllVERSESFromAssets(String filename) throws IOException {
		InputStream inputStream = this.getResources().getAssets()
				.open(filename);
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));

		String line = null;
		int count = 0;

		String expectedLine = null;
		while ((line = reader.readLine()) != null) {
			count++;
			expectedLine = expectedLine + line + "\n";

		}

		count = lastline;
		return expectedLine.substring(4);
	}

	private static final TextView[] views = {

	};

	public String selectTHOUGHTFromAssets(String filename, int number)
			throws IOException {
		InputStream inputStream = this.getResources().getAssets()
				.open(filename);
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));

		String line = null;

		int lineCounter = 0;

		String expectedLine = null;
		readTAGSFromAssets("TAGS.txt");
		while ((line = reader.readLine()) != null) {
			lineCounter++;
			if (lineCounter == number) {

				expectedLine = line;
			}

		}
		return expectedLine;
	}

	public void next() {
		try {
			String end = "\\";
			if (lastline == lineCounter) {
				lineCounter = 1;
			}
			String verse = readVERSESFromAssets(linenumber + ".txt");
			verseview.setText("");
			commentview.setText("");
			if (verse.contains("\\")) {
				verseview.setText(verse.substring(0, verse.indexOf(end.trim()))
						+ "\n"
						+ "\n"
						+ verse.substring(verse.indexOf(end.trim()) + 1,
								verse.length()));
				// commentview.setText(verse.substring(verse.indexOf(end.trim())+1,verse.length()));
			} else {
				verseview.setText(verse);

			}
			if (verseview.getText().length() < 2) {
				verseview.setText("END");
			}
			lineCounter++;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public String readTHOUGHTTAGSFromAssets(String tagfile, String tag)
			throws IOException {
		InputStream inputStream = this.getResources().getAssets().open(tagfile);
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));

		String line = "";

		int lineCounter = 0;

		String expectedLine = null;

		while ((line = reader.readLine()) != null) {
			lineCounter++;

			if (line.toLowerCase().contains(
					tag.substring(tag.indexOf(".") + 1, tag.indexOf(","))
							.trim().toLowerCase())) {
				expectedLine = expectedLine
						+ selectTHOUGHTFromAssets("THOUGHTS.txt", lineCounter)
						+ "\n";
			}

		}
		return expectedLine;
	}

	public String readTHOUGHTTAGSFromAssets1(String tagfile, String tag)
			throws IOException {
		InputStream inputStream = this.getResources().getAssets().open(tagfile);

		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));

		String line = "";

		int lineCounter = 0;

		String expectedLine = "";

		while ((line = reader.readLine()) != null) {
			lineCounter++;

			if (line.toLowerCase().contains(tag.trim().toLowerCase())) {
				expectedLine = expectedLine
						+ selectTHOUGHTFromAssets("THOUGHTS.txt", lineCounter)
						+ "\n";
			}

		}
		return expectedLine;
	}

	public String readTHOUGHTTAGSFromAssets2(String thoughtfile)
			throws IOException {
		InputStream inputStream = this.getResources().getAssets()
				.open(thoughtfile);

		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));

		String line = "";

		int lineCounter = 0;

		String expectedLine = null;

		while ((line = reader.readLine()) != null) {
			lineCounter++;

			if (lineCounter < linemarker) {

				expectedLine = expectedLine + line + "\n";
			}

		}
		return expectedLine;
	}

	public void readTAGSFromAssets(String filename) throws IOException {
		InputStream inputStream = this.getResources().getAssets()
				.open(filename);
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));

		String line = null;

		int lineCounter = 0;

		String expectedLine = null;

		while ((line = reader.readLine()) != null) {
			lineCounter++;
			if (lineCounter == linenumber) {
				expectedLine = line;
			}

		}
		// String html="";
		// String[] parts=expectedLine.split(",");
		// for (int i = 0; i < parts.length; i++) {
		// html=html+"<a href=\"http://sentword.org\">"+parts[i]+"</a>"+",";
		// }
		// html=html;
		// tagsview.setMovementMethod(LinkMovementMethod.getInstance());

		tagsview.setText(expectedLine);
		Pattern pattern = Pattern.compile("([a-zA-Z])\\w+");

		Linkify.addLinks(tagsview, pattern,
				"com.package.something://action-to-perform/id-that-might-be-needed/");

		tagsview.setMovementMethod(new ScrollingMovementMethod());
		tagsview.setMovementMethod(LinkMovementMethod.getInstance());
	}

	public String randromreadfromtags(String tagfile, String tag)
			throws IOException {
		InputStream inputStream2 = this.getResources().getAssets()
				.open(tagfile);
		BufferedReader reader2 = new BufferedReader(new InputStreamReader(
				inputStream2));
		BufferedReader reader3 = new BufferedReader(new InputStreamReader(
				inputStream2));

		String line = null;
		String expectedLine = "hhhhhh";
		String searchkey = tag.toUpperCase();
		int counter = 0;
		int counter1 = 0;
		int randomthought = 0;
		while ((line = reader2.readLine()) != null) {

			if (line.toUpperCase().contains(searchkey.trim())) {
				counter1++;
			}

		}
		randomthought = (int) (Math.random() * counter1 + 1);

		return randomsub("TAGS.txt", String.valueOf(randomthought), searchkey);
	}

	public String randomsub(String filename, String tagrandomnumber,
			String thesearchkey) throws IOException {
		InputStream inputStream2 = this.getResources().getAssets()
				.open(filename);
		BufferedReader reader2 = new BufferedReader(new InputStreamReader(
				inputStream2));
		BufferedReader reader3 = new BufferedReader(new InputStreamReader(
				inputStream2));

		int counter1 = 0;
		String line = "";
		int counter = 0;
		String searchkey = thesearchkey.toUpperCase();
		int randomthought = Integer.parseInt(tagrandomnumber);

		while ((line = reader3.readLine()) != null) {
			counter++;
			if (line.toUpperCase().contains(searchkey.trim())) {
				counter1++;
				if (counter1 == randomthought) {
					linenumber = counter;
				}
			}

		}
		return selectTHOUGHTFromAssets("THOUGHTS.txt", linenumber);
	}

	public String readAllTHOUGHTSFromAssets(String filename) throws IOException {
		InputStream inputStream = this.getResources().getAssets()
				.open(filename);
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));
		String line = null;
		int counter = 0;
		String expectedLine = null;
		while ((line = reader.readLine()) != null) {
			expectedLine = expectedLine + line + "\n";
			counter++;
			if (counter == 150) {
				break;
			}
		}
		return expectedLine;
	}

	public String displayalltags() throws IOException {
		InputStream inputStream2 = this.getResources().getAssets()
				.open("TAGS.txt");
		BufferedReader reader2 = new BufferedReader(new InputStreamReader(
				inputStream2));

		String line = null;
		String expectedLine = "";
		int counter = 0;
		int counter1 = 0;
		int number = 1;
		while ((line = reader2.readLine()) != null) {
			String[] parts = line.split(",");
			for (int i = 0; i < parts.length; i++) {
				if (parts[i].contains(".")) {
					parts[i] = parts[i].substring(parts[i].indexOf("."));
				}
				if (expectedLine.toLowerCase().contains(parts[i].toLowerCase())) {
					Log.i("message", "it is there");
				} else {
					expectedLine = expectedLine + number++ + parts[i] + "\n";
				}
			}

		}

		return expectedLine;

	}

	public String ALLTAGS() throws IOException {
		InputStream inputStream2 = this.getResources().getAssets()
				.open("ALLTOPICSS.txt");
		BufferedReader reader2 = new BufferedReader(new InputStreamReader(
				inputStream2));

		String line = null;
		String expectedLine = "";
		int counter = 0;
		int counter1 = 0;
		int number = 1;
		while ((line = reader2.readLine()) != null) {
			expectedLine = expectedLine + line + "\n";
		}

		return expectedLine;

	}

	@Override
	protected void onResume() {
		super.onResume();
		RelativeLayout mylayout = (RelativeLayout) findViewById(R.id.Rlayout);
		mylayout.getBackground().setAlpha(50);
		// tdatasource.open();
	}

	@Override
	protected void onPause() {
		super.onPause();

		// onBackPressed();
		// finish();
		// System.exit(0);
		// tdatasource.close();
	}

	public void GENERATE() throws IOException {
		start = 0;
		int San, Chlor, Mel, Phleg;
		if (username == true) {
			String option = "";
			San = Integer.parseInt(temperament.trim().substring(1, 2));
			Chlor = Integer.parseInt(temperament.trim().substring(4, 5));
			Mel = Integer.parseInt(temperament.trim().substring(7, 8));
			Phleg = Integer.parseInt(temperament.trim().substring(10));

			List<String> myoptions = new ArrayList<String>();
			Collections.sort(myoptions);
			String preview[] = myoptions.toArray(new String[myoptions.size()]);
			int selector = ((int) (Math.random() * 10 + 1));
			if (selector > 7) {
				option = preview[2];
			} else {
				option = preview[1];
			}

			String topic = "RANDOM RECORDS";
			String topicarray[] = ALLTAGS().split("\n");
			for (int i = 0; i < topicarray.length; i++) {
				if (topicarray[i].contains(option)
						|| topicarray[i].contains("0")) {
					topic = topicarray[i].replaceAll("\\d", "") + ",";
				}
			}
			topicarray = topic.split(",");
			int selector1 = ((int) (Math.random() * topicarray.length));
			topicalview.setText(topicarray[selector1]);

		}

		if (b1.getText().toString().contains(THOUGHTS)) {

			if (topicalview.getText().toString().toLowerCase()
					.contains("random")) {
				thoughtview.setText(readTHOUGHTSFromAssets("THOUGHTS.txt"));
			} else {

				thoughtview.setText(randromreadfromtags("TAGS.txt", topicalview
						.getText().toString().trim()));
			}
			// topicalview.getText().toString().substring(0,topicalview.getText().toString().indexOf("{")
			lineCounter = 1;
			verseview.setText("");
			commentview.setText("");

			readTAGSFromAssets("TAGS.txt");
			next();

		}
		if (b1.getText().toString().contains(COMMENTS)) {
			String end = "\\";
			String verse;
			String expectedLine = "";

			if (topicalview.getText().toString().toLowerCase()
					.contains("random")) {
				commentview.setText("related thought: "
						+ readTHOUGHTSFromAssets("THOUGHTS.txt"));
				verse = readAVERSESandCOMMENT(linenumber + ".txt");
				verseview
						.setText(verse.substring(0, verse.indexOf(end.trim())));
				thoughtview.setText(verse.substring(
						verse.indexOf(end.trim()) + 1, verse.length()));

				// expand verse
				if (verseview.getText().toString().length() < 3) {
					verseview.setText("Please add a comment");
				} else {
					if (verseview.getText().toString().length() < 30) {
						expectedLine = readBible(verseview.getText().toString());
						verseview.setText(expectedLine);
					}

				}

			} else
			// comments displayed by tags
			{

				commentview.setText("related thought: "
						+ randromreadfromtags("TAGS.txt", topicalview.getText()
								.toString().trim()));
				verse = readAVERSESandCOMMENT(linenumber + ".txt");

				verseview
						.setText(verse.substring(0, verse.indexOf(end.trim())));

				thoughtview.setText(verse.substring(
						verse.indexOf(end.trim()) + 1, verse.length()));

				if (verseview.getText().toString().length() < 3) {
					verseview.setText("Please add a comment");
				} else {
					if (verseview.getText().toString().length() < 30) {
						expectedLine = readBible(verseview.getText().toString());
						verseview.setText(expectedLine);
					}

				}

			}
			readTAGSFromAssets("TAGS.txt");

		}
		if (b1.getText().toString().contains(VERSES)) {
			String end = "\\";
			String verse;
			String expectedLine = "";

			if (topicalview.getText().toString().toLowerCase()
					.contains("random")) {
				commentview.setText("related thought: "
						+ readTHOUGHTSFromAssets("THOUGHTS.txt"));
				verse = readAVERSESandCOMMENT(linenumber + ".txt");
				thoughtview.setText(verse.substring(0,
						verse.indexOf(end.trim())));
				verseview.setText(verse.substring(
						verse.indexOf(end.trim()) + 1, verse.length()));
				if (thoughtview.getText().toString().length() < 3) {
					thoughtview.setText("Please add a bible verse");
				} else {
					if (thoughtview.getText().toString().length() < 30) {
						expectedLine = readBible(thoughtview.getText()
								.toString());
						thoughtview.setText(expectedLine);
					}

				}

			} else
			// comments displayed by tags
			{

				commentview.setText("related thought: "
						+ randromreadfromtags("TAGS.txt", topicalview.getText()
								.toString().trim()));
				verse = readAVERSESandCOMMENT(linenumber + ".txt");

				thoughtview.setText(verse.substring(0,
						verse.indexOf(end.trim())));
				verseview.setText(verse.substring(
						verse.indexOf(end.trim()) + 1, verse.length()));

				if (thoughtview.getText().toString().length() < 3) {
					thoughtview.setText("Please add a bible verse");
				} else {
					if (thoughtview.getText().toString().length() < 30) {
						expectedLine = readBible(thoughtview.getText()
								.toString());
						thoughtview.setText(expectedLine);
					}

				}

			}
			readTAGSFromAssets("TAGS.txt");

		}

	}

	@Override
	public void onCreateContextMenu(ContextMenu menu, View v,
			ContextMenuInfo menuInfo) {
		super.onCreateContextMenu(menu, v, menuInfo);
		if (v.getId() == R.id.textView4) {
			String tags[] = tagsview.getText().toString().split(",");
			for (int i = 0; i < tags.length; i++) {
				if (tags[i].contains(".")) {
					menu.add(tags[i].substring(tags[i].indexOf(".")));
				} else if (tags[i].contains("{")) {
					menu.add(tags[i].subSequence(0, tags[i].indexOf("{")));
				} else {
					menu.add(tags[i]);

				}
			}
			topical = 1;
		} else if (v.getId() == R.id.textView12) {
			menu.add("RANDOM RECORDS");
			try {
				String tags[] = ALLTAGS().split("\n");
				for (int i = 0; i < tags.length; i++) {
					menu.add(tags[i]);
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
			topical = 2;
		}
		if (v.getId() == R.id.textView1) {
			try {
				String thoughts[] = readAllTHOUGHTSFromAssets("THOUGHTS.txt")
						.split("\n");
				for (int i = 0; i < thoughts.length; i++) {
					menu.add(thoughts[i]);
				}
				topical = 3;
			} catch (IOException e) {
				e.printStackTrace();
			}

		}
		if (v.getId() == R.id.button2) {

			String subject[] = { THOUGHTS, COMMENTS, VERSES };
			for (int i = 0; i < subject.length; i++) {
				menu.add(subject[i]);
			}
			topical = 4;

		}
		if (v.getId() == R.id.button1) {
			String tags[] = tagsview.getText().toString().split(",");
			for (int i = 0; i < tags.length; i++) {
				if (tags[i].contains(".")) {
					menu.add(tags[i].substring(tags[i].indexOf(".")));
				} else if (tags[i].contains("{")) {
					menu.add(tags[i].subSequence(0, tags[i].indexOf("{")));
				} else {
					menu.add(tags[i]);

				}

			}
			menu.add("show all verses");
			menu.add("suggested responses");
			menu.add("other suggested responses");
			menu.add("Select a Thought");
			// menu.add("SHOW SUGGESTED SCRIPTURES");

			topical = 1;
		}
	}

	@Override
	public boolean onContextItemSelected(MenuItem item) {
		if (topical == 1) {

			String theitem = item.toString().trim();
			if (theitem.contains("show all verses")) {
				Dialog d = new Dialog(MainActivity.this);
				d.setContentView(R.layout.dialogdisplay);
				d.setTitle(thoughtview.getText().toString());

				final TextView allverse = (TextView) d
						.findViewById(R.id.versedisplay);
				allverse.setMovementMethod(new ScrollingMovementMethod());
				try {
					allverse.setText(readAllVERSESFromAssets(linenumber
							+ ".txt"));
					d.show();
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			} else if (theitem.contains("other suggested responses")) {
				start = 0;

				String tags[] = tagsview.getText().toString().split(",");

				Dialog d = new Dialog(MainActivity.this);
				d.setContentView(R.layout.suggestionbox);
				d.setTitle("SUGGESTED REFERENCES");

				final TextView suggestions = (TextView) d
						.findViewById(R.id.suggestions);

				// String verse = verseview.getText().toString();
				String thought = thoughtview.getText().toString();
				String topic = tags[start];
				if (topic.contains(".")) {
					topic = topic.substring(topic.indexOf(".") + 1);
				}

				try {

					suggestions
							.setText(thought
									+ "\n"
									+ "\n"
									+ topic
									+ "\n"
									+ showconcordanceverses(topic.trim()
											.toUpperCase()));
					Pattern pattern = Pattern
							.compile("[\\dA-Za-z]+[a-z]+\\d+:+\\d+");

					Linkify.addLinks(suggestions, pattern,
							"com.package.name://action-to-perform/id-that-might-be-needed/");

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Button B = (Button) d.findViewById(R.id.NextSuggestions);
				B.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						start++;
						String thought = thoughtview.getText().toString();
						String tags[] = tagsview.getText().toString()
								.split(",");

						if (start > tags.length - 1) {
							start = 0;
						}
						String mytopic = tags[start];
						if (mytopic.contains(".")) {
							mytopic = mytopic.substring(mytopic.indexOf(".") + 1);
						}
						if (mytopic.trim().length() < 2) {
							mytopic = tags[0];
						}

						try {
							suggestions.setText(thought
									+ "\n"
									+ "\n"
									+ mytopic
									+ "\n"
									+ "\n"
									+ showconcordanceverses(mytopic.trim()
											.toUpperCase()));
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						Pattern pattern = Pattern
								.compile("[\\dA-Za-z]+[a-z]+\\d+:+\\d+");

						Linkify.addLinks(suggestions, pattern,
								"com.package.name://action-to-perform/id-that-might-be-needed/");
					}
				});
				suggestions.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						if (suggestions.getSelectionStart() == -1
								&& suggestions.getSelectionEnd() == -1) {
							// Toast.makeText(getApplicationContext(),
							// "You clicked outside the link",
							// Toast.LENGTH_SHORT).show();
						} else {
							String lineee = "Gen.5.32	5";
							String line0 = lineee.substring(8, 9);
							int start = suggestions.getSelectionStart();
							int end = suggestions.getSelectionEnd();
							selected = suggestions.getText().toString()
									.substring(start, end);
							// selected=selected.substring(0,selected.indexOf(" "));
							// String theparts[]=selected.split("	");
							// selected=selected.replace(",","").trim();
							// selected=selected.replace("-","").trim();

							// selected=selected.substring(0,selected.indexOf("."))+" "+
							// selected.substring(selected.indexOf(".")+1).replace(".",
							// ":");
							Intent i = new Intent(MainActivity.this,
									Biblepopup.class);
							// tv.setText(readBible(selected));
							// d.show();
							try {
								i.putExtra("VERSE", (selected));
								i.putExtra("thought", thoughtview.getText()
										.toString());
								startActivity(i);
								Toast.makeText(getApplicationContext(),
										"Clicked: " + readBible(selected),

										Toast.LENGTH_LONG).show();
							} catch (IOException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

						}

					}
				});
				d.show();
				suggestions.setMovementMethod(new ScrollingMovementMethod());
				suggestions.setMovementMethod(LinkMovementMethod.getInstance());

			} else if (theitem.contains("suggested responses")) {

				Dialog d = new Dialog(MainActivity.this);
				d.setContentView(R.layout.suggestionbox);
				d.setTitle("SUGGESTED REFERENCES");
				String mysuggestion = "Please find suggested responses as follows"
						+ "\n"
						+ "1) Use The Cross reference Bible to find other verses related to the respond verses already given"
						+ "\n"
						+ "2) Use the tags, topics (in orange colour) to locate verses related to the tags from the bible concordance"
						+ "\n"
						+ "Cross reference Bible can be accessed from the 'BIBLE' button the main menu"
						+ "\n"
						+ "Bible Concordance can be accessed from the 'BIBLE QUOTES' button from the main menu";

				final TextView suggestions = (TextView) d
						.findViewById(R.id.suggestions);
				Button B = (Button) d.findViewById(R.id.NextSuggestions);
				String verse = verseview.getText().toString();
				String thought = thoughtview.getText().toString();
				createsuggestions(suggestions);

				B.setOnClickListener(new OnClickListener() {

					@Override
					public void onClick(View arg0) {
						createsuggestions(suggestions);
					}

				});
				d.show();
			} else if (theitem.contains("Select a Thought")) {
				selectathought();

			} else {
				String bundlethoughts = "";
				try {
					bundlethoughts = readTHOUGHTTAGSFromAssets1("TAGS.txt",
							theitem);
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				Intent i = new Intent(MainActivity.this, second.class);
				i.putExtra("THOUGHTS", bundlethoughts);
				i.putExtra("title", theitem);
				startActivity(i);
			}
		} else if (topical == 2) {
			topicalview.setText(item.toString().replaceAll("\\d", ""));
			try {
				GENERATE();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} else if (topical == 3) {
			thoughtview.setText(item.toString());
			linenumber = Integer.parseInt(item.toString().trim()
					.substring(0, item.toString().indexOf(".")));
			b1.setText("THOUGHTS");
			try {
				readTAGSFromAssets("TAGS.txt");
			} catch (IOException e) {
				e.printStackTrace();
			}
		} else if (topical == 4) {
			b1.setText(item.toString());
		}
		return super.onContextItemSelected(item);

	}

	public void createsuggestions(final TextView suggestions) {
		showreferences = true;
		next();
		String verse = verseview.getText().toString();
		String thought = thoughtview.getText().toString();

		// verse = readVERSESFromAssets(linenumber + ".txt");;
		// suggestions.setText(verseview.getText().toString());
		// suggestions.setTextSize(suggestions.getTextSize());
		if (verse.contains(":")) {
			suggestions.setText(thought + "\n" + verse + "\n"
					+ readline(versepatternsearch(verse)));
		} else {
			suggestions.setText("End of this list of suggestions");
		}
		showreferences = false;
		Pattern pattern3 = Pattern.compile("[1-3][a-zA-Z]+\\.+\\d\\.\\d");
		Pattern pattern4 = Pattern.compile("[1-3][a-zA-Z]+\\.+\\d+\\d+\\.\\d");
		Pattern pattern5 = Pattern.compile("[1-3][a-zA-Z]+\\.+\\d+\\.\\d\\d");
		Pattern pattern6 = Pattern.compile("[1-3][a-zA-Z]+\\.+\\d+\\.\\d\\d");

		Pattern pattern = Pattern.compile("[a-zA-Z]+\\.+\\d+\\.\\d");
		Pattern pattern0 = Pattern.compile("[a-zA-Z]+\\.+\\d+\\d+\\.\\d");
		Pattern pattern1 = Pattern.compile("[a-zA-Z]+\\.+\\d+\\.\\d\\d");
		Pattern pattern2 = Pattern.compile("[a-zA-Z]+\\.+\\d+\\.\\d\\d");

		Linkify.addLinks(suggestions, pattern,
				"com.package.name://action-to-perform/id-that-might-be-needed/");
		Linkify.addLinks(suggestions, pattern0,
				"com.package.name://action-to-perform/id-that-might-be-needed/");
		Linkify.addLinks(suggestions, pattern1,
				"com.package.name://action-to-perform/id-that-might-be-needed/");
		Linkify.addLinks(suggestions, pattern2,
				"com.package.name://action-to-perform/id-that-might-be-needed/");
		Linkify.addLinks(suggestions, pattern3,
				"com.package.name://action-to-perform/id-that-might-be-needed/");
		Linkify.addLinks(suggestions, pattern4,
				"com.package.name://action-to-perform/id-that-might-be-needed/");
		Linkify.addLinks(suggestions, pattern5,
				"com.package.name://action-to-perform/id-that-might-be-needed/");
		Linkify.addLinks(suggestions, pattern6,
				"com.package.name://action-to-perform/id-that-might-be-needed/");

		suggestions.setMovementMethod(new ScrollingMovementMethod());
		suggestions.setMovementMethod(LinkMovementMethod.getInstance());

		suggestions.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View arg0) {
				suggestionbycrossreference(suggestions);
				// second thesecond=new second();
				// suggestions.setText(thesecond.showconcordanceverses(tagsview.getText().toString().substring(start)))

			}

			public void suggestionbycrossreference(final TextView suggestions) {
				if (suggestions.getSelectionStart() == -1
						&& suggestions.getSelectionEnd() == -1) {
					// Toast.makeText(getApplicationContext(),
					// "You clicked outside the link",
					// Toast.LENGTH_SHORT).show();
				} else {
					String lineee = "Gen.5.32	5";
					String line0 = lineee.substring(8, 9);
					int start = suggestions.getSelectionStart();
					int end = suggestions.getSelectionEnd();
					selected = suggestions.getText().toString()
							.substring(start - 1, end + 1);
					// String theparts[]=selected.split("	");
					selected = selected.replace(",", "").trim();
					selected = selected.replace("-", "").trim();

					selected = selected.substring(0, selected.indexOf("."))
							+ " "
							+ selected.substring(selected.indexOf(".") + 1)
									.replace(".", ":");
					Intent i = new Intent(MainActivity.this, Biblepopup.class);
					// tv.setText(readBible(selected));
					// d.show();
					try {
						i.putExtra("VERSE", readBible(selected));
						i.putExtra("thought", thoughtview.getText().toString());
						startActivity(i);
						Toast.makeText(getApplicationContext(),
								"Clicked: " + readBible(selected),

								Toast.LENGTH_LONG).show();
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}

				}
			}
		});
	}

	public String readAlternateverse() {
		return COMMENTS;

	}

	public String bible(String BOOK, String CHAPTER, String VERSE, String END)
			throws IOException {
		InputStream inputStream = this.getResources().getAssets()
				.open(BOOK + ".txt");
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));

		int linecount = 0;
		String expectedLine = "";
		String line = "";
		int counter = 0;
		int limiter = 0;
		boolean begin = false;
		boolean endverse = false;
		if (!END.equals("")) {
			limiter = Integer.parseInt(END) - Integer.parseInt(VERSE);
		}
		while ((line = reader.readLine()) != null) {
			linecount++;
			if (BOOK.toLowerCase().contains("psal")) {

				if (line.contains("PSALM " + CHAPTER)) {
					begin = true;
				}
				if (begin == true) {
					if (line.trim().startsWith(VERSE + " ")) {

						expectedLine = BOOK + CHAPTER + ":" + line;
						break;

					}
				}

			} else {
				if (line.contains("CHAPTER " + CHAPTER)) {
					begin = true;
					// expectedLine=line;break;

				}

				if (begin == true) {
					if (line.trim().startsWith(VERSE + " ") || endverse == true) {
						if (END.equals("")) {
							expectedLine = BOOK + CHAPTER + ":" + line;
							break;
						} else {
							endverse = true;
							expectedLine = expectedLine + line;
							counter++;
							if (counter == limiter + 1) {
								expectedLine = BOOK + CHAPTER + ":"
										+ expectedLine;
								begin = false;
								break;
							}
						}

					}

				}
			}
		}
		return expectedLine;
	}

	public boolean containsnumber(String word) {
		if (word.matches(".*\\d.*")) {
			return true;
		} else {
			return false;
		}
	}

	public String showconcordanceverses(String word) throws IOException {
		word = word.toUpperCase();
		InputStream inputStream = this.getResources().getAssets()
				.open(word.trim().substring(0, 1) + ".txt");
		BufferedReader reader = new BufferedReader(new InputStreamReader(
				inputStream));
		String line = null;
		int counter = 0;
		boolean begin = false;
		String expectedLine = "";

		while ((line = reader.readLine()) != null) {

			if (line.toLowerCase().contains(word.toLowerCase())) {
				begin = true;

			}
			if (begin == true) {
				if (line.contains(":")) {

					expectedLine = expectedLine + readBible(line) + "\n" + "\n";
					counter++;
				}
				if (counter > 1 & !line.contains(":")) {
					begin = false;
					break;
				}
			}

		}
		return expectedLine;
	}

	public void selectathought() {
		final String searchallresults = "";
		AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);

		{
			builder.setTitle("SEARCH WORDS IN THOUGHTS");
		}

		final EditText input = new EditText(MainActivity.this);
		input.setInputType(InputType.TYPE_CLASS_NUMBER);
		input.setHint("type a thought number");

		// input.setInputType(InputType.TYPE_CLASS_NUMBER);
		builder.setView(input);

		builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {

			@Override
			public void onClick(DialogInterface dialog, int which) {

				try {

					{
						int myeditnumber = Integer.parseInt(input.getText()
								.toString());
						if (myeditnumber > 1513) {
							myeditnumber = 1513;
						}
						linenumber = myeditnumber;

						String thethought = selectTHOUGHTFromAssets(
								"THOUGHTS.txt", linenumber);

						// next();

						thoughtview.setText(thethought);

					}

				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}

			}
		});
		builder.setNegativeButton("Cancel",
				new DialogInterface.OnClickListener() {

					@Override
					public void onClick(DialogInterface dialog, int which) {
						dialog.cancel();

					}
				});
		builder.show();
		;
	}

	public String readline(String myverse) {
		allocation allot = new allocation();
		String preparedbook = allot.books(myverse).trim();
		String realbook = preparedbook.substring(0, preparedbook.indexOf("."))
				.trim();
		int position = Arrays.asList(nonabbbook).indexOf(realbook);

		// String fullbook=myverse.trim().substring(0,
		// myverse.trim().indexOf("."));
		String ABBBOOK = allocation.crossrefBOOKS[position];
		String VERSE = myverse.substring(myverse.indexOf(":") + 1);
		String prechapter = myverse.substring(0, myverse.indexOf(":"));
		String CHAPTER = prechapter.replaceAll("[^\\d]", "");
		// String theVERSE=VERSE.substring(0, VERSE.indexOf("."));
		String verse = ABBBOOK + "." + CHAPTER + "." + VERSE;
		// String verse="Gen.5.3";
		String formatlines = "";
		String line0 = "";
		String Crossbook = "Cross" + realbook;
		try {
			InputStream inputStream = this.getResources().getAssets()
					.open(Crossbook + ".txt");
			BufferedReader reader2 = new BufferedReader(new InputStreamReader(
					inputStream));
			String line, theline = null;
			theline = String.valueOf(reader2.readLine());
			int line4 = 0;
			int chapter = 0;

			while ((line = reader2.readLine()) != null) {
				line4++;

				if (line.trim().startsWith(verse)) {
					String lineee = "Phil.4.8	1Tim.3.7-1Tim.3.8";
					line0 = lineee.substring(8, 9);
					// System.out.println(line0);
					// System.out.println(line0.replaceFirst(line0,
					// "________"));
					String lineformat = line.replaceFirst(line0, "________");

					String line1 = lineformat
							.substring(lineformat.indexOf(".") + 1);
					String line2 = line1.substring(line1.indexOf(".") + 4);

					String line3 = line2.replaceAll("_", " ").trim();
					formatlines = formatlines + "," + line3;
					System.out.println(line3);

					// System.out.println(line.trim().substring(0,
					// line.indexOf(".")));
					// System.out.println(line);
				}
			}
		} catch (FileNotFoundException ex) {
			Logger.getLogger(Crossreference.class.getName()).log(Level.SEVERE,
					null, ex);
		} catch (IOException ex) {
			Logger.getLogger(Crossreference.class.getName()).log(Level.SEVERE,
					null, ex);
		}
		return formatlines;
	}

	public String versepatternsearch(String verse) {
		Pattern pattern = Pattern.compile(":+\\d+\\d");
		Pattern pattern0 = Pattern.compile(":+\\d+\\d");
		Pattern pattern1 = Pattern.compile(":+\\d");
		Pattern pattern2 = Pattern.compile(":+\\d");
		Pattern pattern3 = Pattern.compile(":+\\d+\\d");
		Pattern pattern4 = Pattern.compile(":+\\d");
		Pattern pattern5 = Pattern.compile(":+\\d");
		Pattern pattern6 = Pattern.compile(":+\\d+\\d");

		Pattern pattern7 = Pattern.compile(":+\\d");
		Pattern pattern8 = Pattern.compile(":+\\d+\\d");
		Pattern pattern9 = Pattern.compile(":+\\d+\\d+\\d+");

		Pattern pattern00 = Pattern.compile(":+\\d+\\d");
		Pattern pattern10 = Pattern.compile(":+\\d+\\d");
		Pattern pattern11 = Pattern.compile(":+\\d");
		Pattern pattern12 = Pattern.compile(":+\\d");
		Pattern pattern13 = Pattern.compile(":+\\d+\\d");
		Pattern pattern14 = Pattern.compile(":+\\d");
		Pattern pattern15 = Pattern.compile(":+\\d");
		Pattern pattern16 = Pattern.compile(":+\\d+\\d");

		Pattern pattern17 = Pattern.compile(":+\\d");
		Pattern pattern18 = Pattern.compile(":+\\d+\\d");
		Pattern pattern19 = Pattern.compile(":+\\d+\\d+\\d+");

		String right = "";
		Pattern[] thispattern = { pattern, pattern0, pattern1, pattern2,
				pattern3, pattern4, pattern5, pattern6, pattern7, pattern8,
				pattern9, pattern, pattern00, pattern11, pattern12, pattern13,
				pattern14, pattern15, pattern16, pattern17, pattern18,
				pattern19, };
		Matcher m = null;
		for (int i = 0; i < thispattern.length; i++) {
			m = thispattern[i].matcher(verse);
			if (m.find()) {
				right = m.group(0);
				break;
			}

		}

		// System.out.println(matcher.group(1));
		if (verse.contains(":")) {
			if (verse.substring(0, verse.indexOf(":")).contains(")")) {
				verse = verse.substring(verse.indexOf(")") + 1);
			}
			if (verse.substring(0, verse.indexOf(":")).contains(".")) {
				verse = verse.substring(verse.indexOf(".") + 1);
			}

			return verse.substring(0, verse.indexOf(":")) + right;
		} else {
			return null;
		}
	}

	public String nonabbbook[] = { "Genesis", "Exodus", "Leviticus", "Numbers",
			"Deuteronomy", "Joshua", "Judges", "Ruth", "1Samuel", "2Samuel",
			"1Kings", "2Kings", "1Chronicles", "2Chronicles", "Ezra",
			"Nehemiah", "Esther", "Job", "Psalms", "Proverbs", "Ecclesiastes",
			"Song of Songs", "Isaiah", "Jeremiah", "Lamentations", "Ezekiel",
			"Daniel", "Hosea", "Joel", "Amos", "Obadiah", "Jonah", "Micah",
			"Nahum", "Habakkuk", "Zephaniah", "Haggai", "Zechariah", "Malachi",
			"Matthew", "Mark", "Luke", "John", "Acts", "Romans",
			"1Corinthians", "2Corinthians", "Galatians", "Ephesians",
			"Philippians", "Colossians", "1Thessalonians", "2Thessalonians",
			"1Timothy", "2Timothy", "Titus", "Philemon", "Hebrews", "James",
			"1Peter", "2Peter", "1John", "2John", "3John", "Jude", "Revelation"

	};
	private String whattosubmit;

	public void addthought() {
		// Dialog d=new Dialog(MainActivity.this);
		// d.setContentView(R.layout.fulladditions);
		// whattosubmit="thought";
		// d.show();
		Toast.makeText(MainActivity.this, "it works", 5000).show();

	}

	public void addverse() {
		Dialog d = new Dialog(MainActivity.this);
		d.setContentView(R.layout.fulladditions);
		EditText et = (EditText) d.findViewById(R.id.editthought);
		et.setText(thoughtview.getText().toString());
		EditText et1 = (EditText) d.findViewById(R.id.editverse);
		whattosubmit = "verse";

		d.show();
	}

	public void addcomment() {
		Dialog d = new Dialog(MainActivity.this);
		d.setContentView(R.layout.fulladditions);
		EditText et = (EditText) d.findViewById(R.id.editthought);
		EditText et1 = (EditText) d.findViewById(R.id.editverse);

		et.setText(thoughtview.getText().toString());
		et1.setText(verseview.getText().toString());
		EditText et2 = (EditText) d.findViewById(R.id.editcomment);
		whattosubmit = "comment";

		d.show();
	}

	public void helpful() {
		ParseObject helpful = new ParseObject("HELPFULTHOUGHTS");
		helpful.put(
				"thought",
				thoughtview
						.getText()
						.toString()
						.substring(0,
								thoughtview.getText().toString().indexOf(".")));
		helpful.put("helpful", 1);
		if (sharedpreferencesuser.getBoolean("signedin", true)) {
			helpful.put("username",
					sharedpreferencesuser.getString("username", ""));
		} else {
			helpful.put("username", "anonymous");

		}
		helpful.saveEventually();
	}

	public void demote() {
		ParseObject demote = new ParseObject("DEMOTEDTHOUGHTS");
		demote.put(
				"thought",
				thoughtview
						.getText()
						.toString()
						.substring(0,
								thoughtview.getText().toString().indexOf(".")));
		demote.put("demote", 1);
		if (sharedpreferencesuser.getBoolean("signedin", true)) {
			demote.put("username",
					sharedpreferencesuser.getString("username", ""));
		} else {
			demote.put("username", "anonymous");

		}
		demote.saveEventually();
	}

	public void unclear() {
		ParseObject unclear = new ParseObject("UNCLEARTHOUGHTS");
		unclear.put(
				"thought",
				thoughtview
						.getText()
						.toString()
						.substring(0,
								thoughtview.getText().toString().indexOf(".")));
		unclear.put("unclear", 1);
		if (sharedpreferencesuser.getBoolean("signedin", true)) {
			unclear.put("username",
					sharedpreferencesuser.getString("username", ""));
		} else {
			unclear.put("username", "anonymous");

		}
		unclear.saveEventually();
	}

	public void submitforediting(View arg0) {
		Dialog d = new Dialog(MainActivity.this);
		d.setContentView(R.layout.fulladditions);
		EditText editthought = (EditText) d.findViewById(R.id.editthought);
		EditText editverse = (EditText) d.findViewById(R.id.editverse);
		EditText editcomment = (EditText) d.findViewById(R.id.editcomment);

		ParseObject thoughts = new ParseObject("THOUGHTS");
		ParseObject thoughtverse = new ParseObject("THOUGHTVERSE");
		ParseObject versecomment = new ParseObject("VERSECOMMENT");
		ParseObject thoughtcomment = new ParseObject("THOUGHTCOMMENT");

		if (whattosubmit.contains("thought")) {
			if (editthought.getText().toString().trim() != "") {
				thoughts.put("thought", editthought.getText().toString().trim());
				thoughts.put("username",
						sharedpreferencesuser.getString("username", ""));
				thoughts.saveEventually();
				Toast.makeText(MainActivity.this, "Record has been submitted",
						5000).show();
			}
			if (editverse.getText().toString().trim() != "") {
				thoughtverse.put("thoughtverse", editthought.getText()
						.toString().trim()
						+ "." + editverse.getText().toString().trim());
				thoughtverse.put("username",
						sharedpreferencesuser.getString("username", ""));
				thoughtverse.saveEventually();
				Toast.makeText(MainActivity.this, "Record has been submitted",
						5000).show();

				if (editcomment.getText().toString().trim() != "") {
					versecomment.put("versecomment", editverse.getText()
							.toString().trim()
							+ "." + editcomment.getText().toString().trim());
					versecomment.put("username",
							sharedpreferencesuser.getString("username", ""));
					versecomment.saveEventually();
					Toast.makeText(MainActivity.this,
							"Record has been submitted", 5000).show();

				}
			}

			if (editthought.getText().toString().trim() != ""
					&& editcomment.getText().toString().trim() != "") {
				thoughtcomment.put("thoughtcomment", editthought.getText()
						.toString().trim()
						+ editcomment.getText().toString().trim());
				thoughtcomment.put("username",
						sharedpreferencesuser.getString("username", ""));
				thoughtcomment.saveEventually();
				Toast.makeText(MainActivity.this, "Record has been submitted",
						5000).show();

			}
		} else if (whattosubmit.contains("verse")) {
			String thought = editthought.getText().toString().trim();
			if (editverse.getText().toString().trim() != "") {
				thoughtverse.put("thoughtverse",
						thought.substring(0, thought.indexOf(".")) + "."
								+ editverse.getText().toString().trim());
				thoughtverse.put("username",
						sharedpreferencesuser.getString("username", ""));
				thoughtverse.saveEventually();
				Toast.makeText(MainActivity.this, "Record has been submitted",
						5000).show();

				if (editcomment.getText().toString().trim() != "") {
					versecomment.put("versecomment",
							thought.substring(0, thought.indexOf(".")) + "."
									+ editcomment.getText().toString().trim());
					versecomment.put("username",
							sharedpreferencesuser.getString("username", ""));
					versecomment.saveEventually();
					Toast.makeText(MainActivity.this,
							"Record has been submitted", 5000).show();

				}
			}

			if (editthought.getText().toString().trim() != ""
					&& editcomment.getText().toString().trim() != "") {
				thoughtcomment.put("thoughtcomment",
						thought.substring(0, thought.indexOf("."))
								+ editcomment.getText().toString().trim());
				thoughtcomment.put("username",
						sharedpreferencesuser.getString("username", ""));
				thoughtcomment.saveEventually();
				Toast.makeText(MainActivity.this, "Record has been submitted",
						5000).show();

			}
		} else if (whattosubmit.contains("comment")) {
			String thought = editthought.getText().toString().trim();

			if (editcomment.getText().toString().trim() != "") {
				versecomment.put("versecomment", editverse.getText().toString()
						.trim()
						+ "." + editcomment.getText().toString().trim());
				versecomment.put("username",
						sharedpreferencesuser.getString("username", ""));
				versecomment.saveEventually();
				Toast.makeText(MainActivity.this, "Record has been submitted",
						5000).show();

			}

			if (editthought.getText().toString().trim() != ""
					&& editcomment.getText().toString().trim() != "") {
				thoughtcomment.put("thoughtcomment",
						thought.substring(0, thought.indexOf("."))
								+ editcomment.getText().toString().trim());
				thoughtcomment.put("username",
						sharedpreferencesuser.getString("username", ""));
				thoughtcomment.saveEventually();
				Toast.makeText(MainActivity.this, "Record has been submitted",
						5000).show();

			}
		}
		whattosubmit = "";
	}

	@Override
	public void onClick(View arg0) {

	}

	public void useless() {

	}
}
