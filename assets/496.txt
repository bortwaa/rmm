Philipians 2:3-5 \The mind of Christ we have received in Christ always seeks the best of others without regard to self.
Ps 40:14\ Wishing evil for others will only end me in shame and backwardness.
1Cor 12:26 \It is to my advantage if others get what I don�t get because in so doing I never lose any blessing because I always share in whatever blessing they have through my connection to them in the spirit of love which lives in the body of Christ. What I don�t get directly can be gotten indirectly through others
