113.Isa 55:8-9  
1Jo 2:27      \The worth of a thing and the way to do it, is not defined by my thoughts but defined by God's thoughts which is freely accessible though the Spirit of God who is in every child of God
a.John 4:34    \The love we have for doing a work is more important than the gains we perceive we will get from it because things that we do for the sake of our love for them are things that impart strength and life to our lives
b.Pr 12:24\     In all we do, we need to be motivated by the love for work and not the love for rest because with diligence comes worthy rewards
c.Heb 13:2\    Every kind of work God has entrusted to us is what God uses to make us more perfect in walking in His will
Pr 12:27