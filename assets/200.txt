200a)1Co 12:23 And those members of the body, which we think to be less honourable, upon these we bestow more abundant honour; and our uncomely parts have more abundant comeliness. 
200b)1Co 12:24 For our comely parts have no need: but God hath tempered the body together, having given more abundant honour to that part which lacked:
200c)1Co 12:22 Nay, much more those members of the body, which seem to be more feeble, are necessary:
200d)Ec 3:14 I know that, whatsoever God doeth, it shall be for ever: nothing can be put to it, nor any thing taken from it: and God doeth it, that men should fear before him. \Nothing can be added or removed from who I am in God to make me better. I am the best I can ever be, because I am the masterpiece of the best master craftsman.
200e)Col 2:10 And ye are complete in him, which is the head of all principality and power: \My life is complete in Christ and not in anything in or not in me.
200f)Ge 1:26 And God said, Let us make man in our image, after our likeness:...  \I am already as endowed as God is.
200g)1Co 12:26 And whether one member suffer, all the members suffer with it; or one member be honoured, all the members rejoice with it. \All the good things I see in others are also mine, for it belongs to Christ and Christ is mine life.
200h)1Co 3:16 Know ye not that ye are the temple of God, and that the Spirit of God dwelleth in you\ I am endowed with God Himself.
200i)1Co 15:9 For I am the least of the apostles, that am not meet to be called an apostle, because I persecuted the church of God.10 But by the grace of God I am what I am: and his grace which was bestowed upon me was not in vain; but I laboured more abundantly than they all: yet not I, but the grace of God which was with me.
200j)Ro 4:16 Therefore it is of faith, that it might be by grace;... \The grace of God obtained through faith is the greatest endowment.
200k)Ro 9:16 So then it is not of him that willeth, nor of him that runneth, but of God that sheweth mercy.
200l)1Co 1:27 But God hath chosen the foolish things of the world to confound the wise; and God hath chosen the weak things of the world to confound the things which are mighty;
200m)1Co 3:21  Therefore let no man glory in men. For all things are yours
