package com.gloryproject.renewingmymind;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Calendar;

import com.gloryproject.renewingmymind.R;

import android.R.string;
import android.app.Activity;
import android.app.AlarmManager;
import android.app.Dialog;
import android.app.PendingIntent;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.text.method.ScrollingMovementMethod;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

public class MENU extends Activity implements OnClickListener{
	Button thoughts,comments,verses,devotionals,bible,theaboutus,theshareapp,therateapp,thewebsiteapp;
	private int marker=0;
	private String THOUGHTS_DESCRIPTION="The Thoughts Page present mostly NEGATIVE THOUGHTS and appropriate scrpitural responses to each.\n Click 'OK' to navigate to the thoughts pages";
	private String COMMENTS_DESCRIPTION="The Inspiration Quotes Page presennts inspiration quotations and biblical support to each quote.\n Click 'OK' to navigate to the inspirational quotes pages";
	private String VERSES_DESCRIPTION="The Verses Page presents inpirational biblical quotes and meditations on them.\n Click 'OK' to navigate to the verses pages";
	private String DEVOTIONALS_DESCRIPTION="The Devotional Page presents articles on various topics of christian living.\n Click 'OK' to navigate to the devotionals pages";
	private String BIBLE_DESCRIPTION="Click 'OK' to navigate to the CROSS-REFERENCE HOLY BIBLE";
	private String BATTLING_EFFECTIVELY="";
	String option="";
    //private String BATTLING_EFFECTIVELY=
	//private String BATTLING_EFFECTIVELY=
	//private String BATTLING_EFFECTIVELY=
	//private String BATTLING_EFFECTIVELY=
	//private String BATTLING_EFFECTIVELY=
	//private String BATTLING_EFFECTIVELY=
		


@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.menu);
	thoughts=(Button) findViewById(R.id.thoughts);
	comments=(Button) findViewById(R.id.comments);
	verses=(Button) findViewById(R.id.verses);
	devotionals=(Button) findViewById(R.id.devotionals);
	bible=(Button) findViewById(R.id.bible);
	
	theshareapp=(Button) findViewById(R.id.buttonshareapp);
	therateapp=(Button) findViewById(R.id.buttonrateapp);
	theaboutus=(Button) findViewById(R.id.buttonaboutapp);
	thewebsiteapp=(Button) findViewById(R.id.websiteapp);
	
	
	thoughts.setOnClickListener(this);
	comments.setOnClickListener(this);
	verses.setOnClickListener(this);
	devotionals.setOnClickListener(this);
	bible.setOnClickListener(this);
	
	theshareapp.setOnClickListener(this);
	therateapp.setOnClickListener(this);
	theaboutus.setOnClickListener(this);
	thewebsiteapp.setOnClickListener(this);

	thoughts.getBackground().setAlpha(50);
	comments.getBackground().setAlpha(50);
	verses.getBackground().setAlpha(50);
	devotionals.getBackground().setAlpha(50);
	bible.getBackground().setAlpha(50);
	
	theshareapp.getBackground().setAlpha(50);
	therateapp.getBackground().setAlpha(50);
	theaboutus.getBackground().setAlpha(50);
	thewebsiteapp.getBackground().setAlpha(50);
	
	
	RelativeLayout mylayout=(RelativeLayout) findViewById(R.id.menuRlayout);
	mylayout.getBackground().setAlpha(255);
	
	 // get a Calendar object with current time
	 Calendar cal = Calendar.getInstance();
	 // add 5 minutes to the calendar object
	 cal.add(Calendar.AM_PM, 7);
	 Intent intent = new Intent(MENU.this, AlarmReciever.class);
	 intent.putExtra("alarm_message", "O'Doyle Rules!");
	 // In reality, you would want to have a static variable for the request code instead of 192837
	 PendingIntent sender = PendingIntent.getBroadcast(this, 192837, intent, PendingIntent.FLAG_UPDATE_CURRENT);
	 
	 // Get the AlarmManager service
	 AlarmManager am = (AlarmManager) getSystemService(ALARM_SERVICE);
	 am.set(AlarmManager.RTC_WAKEUP, cal.getTimeInMillis(), sender);

	 String userdetails=getIntent().getStringExtra("userdetails");
	 
	
}

@Override
public boolean onCreateOptionsMenu(Menu menu) {
	getMenuInflater().inflate(R.menu.aboutus, menu);
	return true;
}
@Override
public boolean onOptionsItemSelected(MenuItem item) {
	if(item.getItemId()==R.id.aboutus){
		String value="This Application was produced by Sent Word International Ministries(SWIM) as a department of the it's ongoing open source project (Renewing My Mind). Please visit our site to see how you can also be a part of this Project, delivered to us by the Lord. God's Grace be multiplied to You. \n" +
				"Our Page-https://www.facebook.com/pages/Renewing-MY-MIND/103612883122226?ref=hl&ref_type=bookmark \n" +
				"Our Group-https://www.facebook.com/groups/battleforthemind/ \n" +
				"Our Website-https://www.sentword.org";

		Intent i=new Intent(MENU.this,VIEW.class);
		i.putExtra("ABOUTUS", value);
		i.putExtra("TITLE", "ABOUT US");
startActivity(i);
		
	}

if(item.getItemId()==R.id.visitpage){
	Uri uri = Uri.parse("https://www.facebook.com/pages/Renewing-MY-MIND/103612883122226?ref=hl&ref_type=bookmark");
	Intent intent = new Intent(Intent.ACTION_VIEW, uri);
	startActivity(intent);
	
}
if(item.getItemId()==R.id.visitgroup){
	Uri uri = Uri.parse("https://www.facebook.com/groups/battleforthemind/");
	Intent intent = new Intent(Intent.ACTION_VIEW, uri);
	startActivity(intent);
	
}
if(item.getItemId()==R.id.visitsite){
	Uri uri = Uri.parse("https://www.sentword.org");
	Intent intent = new Intent(Intent.ACTION_VIEW, uri);
	startActivity(intent);
}
if(item.getItemId()==R.id.shareapp){
	Intent shareintent = new Intent(Intent.ACTION_SEND);
	shareintent.setType("text/plain");
	shareintent.putExtra(Intent.EXTRA_TEXT, "This application is loaded with rich inspirational materials for the " +
			"renewal and refreshing of the mind, Click the link below to check it out"+"\n"+
			"https://play.google.com/store/apps/details?id=com.gloryproject.renewingmymind");
	shareintent.putExtra(Intent.EXTRA_SUBJECT,
			"PLEASE CHECK THIS APPLICATION");
	startActivity(Intent.createChooser(shareintent, "share"));
}
if(item.getItemId()==R.id.rateapp){
Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.gloryproject.renewingmymind");
Intent intent = new Intent(Intent.ACTION_VIEW, uri);
startActivity(intent);
}
if(item.getItemId()==R.id.updateapp){
Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.gloryproject.renewingmymind");
Intent intent = new Intent(Intent.ACTION_VIEW, uri);
startActivity(intent);
}

	return super.onOptionsItemSelected(item);
}
@Override
public void onClick(View b) {
	Dialog d=new Dialog(MENU.this);
	d.setContentView(R.layout.menudialog);
	final TextView tv=(TextView)d.findViewById(R.id.menudisplay);
	final Button okbutton=(Button) d.findViewById(R.id.menuok);
	//final Button cancelbutton=(Button) d.findViewById(R.id.menucancel);
	
	okbutton.setOnClickListener(this);
	//cancelbutton.setOnClickListener(this);
	
	
	Intent intentthought=new Intent(MENU.this,MainActivity.class);
	Intent intentbible=new Intent(MENU.this,Biblelookup.class);
	Intent intentverse=new Intent(MENU.this,MainActivity.class);
	Intent intentdevotional=new Intent(MENU.this,Devotionals.class);
	Intent intentcomments=new Intent(MENU.this,MainActivity.class);
	Intent intentallthought=new Intent(MENU.this,second.class);
	intentallthought.putExtra("ALLTHOUGHTS", "YES");

	Dialog myd=new Dialog(MENU.this);
	myd.setContentView(R.layout.thoughtsdialog);
	myd.setTitle("THOUGHTS MENU");
	
	final Button random=(Button) myd.findViewById(R.id.random);
	final Button all=(Button) myd.findViewById(R.id.all);
	
	random.setOnClickListener(this);
	all.setOnClickListener(this);
	//Intent i=new Intent(MENU.this,MainActivity.class);

	if(b.getId()==R.id.thoughts){
		//tv.setText(THOUGHTS_DESCRIPTION);
		//marker=1;
		//d.show();
		
		
		//Toast.makeText(MENU.this, "TESTING", 5000).show();
		myd.show();
	}else
	if(b.getId()==R.id.comments){
		tv.setText(COMMENTS_DESCRIPTION);
		marker=2;
		
		myd.setTitle("INSPIRATIONAL QUOTES");
		all.setText("SHOW ALL QUOTES");
		random.setText("SHOW RANDOM QUOTES");
		option="thecomment";
		myd.show();

	}else	
	if(b.getId()==R.id.verses){
		tv.setText(VERSES_DESCRIPTION);
		marker=3;
		myd.setTitle("BIBLE QUOTES");
		myd.setTitle("BIBLE CONCORDANCE");
		all.setText("SHOW ALL VERSES");
		random.setText("SHOW RANDOM VERSES");
		option="theverse";
		myd.show();

	}else
	if(b.getId()==R.id.devotionals){
		tv.setText(DEVOTIONALS_DESCRIPTION);
		marker=4;
		d.setTitle("DEVOTIONALS");
		d.show();

	}else	
	if(b.getId()==R.id.bible){
		tv.setText(BIBLE_DESCRIPTION);
		marker=5;
		d.setTitle("CROSS REFERENCE BIBLE");
		d.show();

	}else
	if(b.getId()==R.id.menuok){
		
		if(marker==1){
			startActivity(intentthought);
		}else
		if(marker==2){
			intentcomments.putExtra("MODE", "QUOTES");
			startActivity(intentcomments);	
		}else
		if(marker==3){
			intentverse.putExtra("VERSEKEY", "VERSES");
			startActivity(intentverse);	
		}else
		if(marker==4){
			startActivity(intentdevotional);
		}else
		if(marker==5){
			startActivity(intentbible);
		}
		
		
	}
    else
		if(b.getId()==R.id.random){
			if(option=="theverse"){
				intentverse.putExtra("VERSEKEY", "VERSES");
				startActivity(intentverse);	
				option="";
			}else
				if(option=="thecomment"){
					intentcomments.putExtra("MODE", "QUOTES");
					startActivity(intentcomments);		
					option="";
				}
			else{
				startActivity(intentthought);
			}
			
		}else
			if(b.getId()==R.id.all){
				if(option=="theverse"){
					Intent i = new Intent(MENU.this, second.class);
					Intent i2 = new Intent(MENU.this, second.class);
					option="";
					try {
						i.putExtra("thoughttag", ALLTOPICS());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					i.putExtra("title", "TOPICS");
					startActivity(i);
					
					
				}else 
					if(option=="thecomment"){
					Intent i = new Intent(MENU.this, second.class);
					Intent i2 = new Intent(MENU.this, second.class);
					option="";
					try {
						i.putExtra("COMMENT", ALLCOMMENT());
					} catch (IOException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
					}
					i.putExtra("title", "COMMENT");
					startActivity(i);
					
					
				}
				else{
				Intent i = new Intent(MENU.this, second.class);
				Intent i2 = new Intent(MENU.this, second.class);
				try {
					i.putExtra("thoughttag", ALLTAGS());
					i.putExtra("title", "TAGS");
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				startActivity(i);
				}
			}
	if(b.getId()==R.id.buttonaboutapp){
		
			String value="This Application was produced by Sent Word International Ministries(SWIM) as a department of the it's ongoing open source project (Renewing My Mind). Please visit our site to see how you can also be a part of this Project, delivered to us by the Lord. God's Grace be multiplied to You. \n" +
					"Our Page-https://www.facebook.com/pages/Renewing-MY-MIND/103612883122226?ref=hl&ref_type=bookmark \n" +
					"Our Group-https://www.facebook.com/groups/battleforthemind/ \n" +
					"Our Website-https://www.sentword.org";

			Intent i=new Intent(MENU.this,VIEW.class);
			i.putExtra("ABOUTUS", value);
			i.putExtra("TITLE", "ABOUT US");
	startActivity(i);
		
	}
	if(b.getId()==R.id.buttonshareapp){
		Intent shareintent = new Intent(Intent.ACTION_SEND);
		shareintent.setType("text/plain");
		shareintent.putExtra(Intent.EXTRA_TEXT, "This application is loaded with rich inspirational materials for the " +
				"renewal and refreshing of the mind, Click the link below to check it out"+"\n"+
				"https://play.google.com/store/apps/details?id=com.gloryproject.renewingmymind");
		shareintent.putExtra(Intent.EXTRA_SUBJECT,
				"PLEASE CHECK THIS APPLICATION");
		startActivity(Intent.createChooser(shareintent, "share"));
	}
	if(b.getId()==R.id.websiteapp){
		Uri uri = Uri.parse("https://www.sentword.org");
		Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		startActivity(intent);
	}
	if(b.getId()==R.id.buttonrateapp){
		//Uri uri = Uri.parse("https://play.google.com/store/apps/details?id=com.gloryproject.renewingmymind");
		//Intent intent = new Intent(Intent.ACTION_VIEW, uri);
		//startActivity(intent);
		
		Intent intent=new Intent(MENU.this,PopupBible.class);
		startActivity(intent);
	}
	
}
@Override
protected void onResume() {
	super.onResume();
	thoughts.getBackground().setAlpha(50);
	comments.getBackground().setAlpha(50);
	verses.getBackground().setAlpha(50);
	devotionals.getBackground().setAlpha(50);
	bible.getBackground().setAlpha(50);
	
	RelativeLayout mylayout=(RelativeLayout) findViewById(R.id.menuRlayout);
	mylayout.getBackground().setAlpha(255);
	

}
public String ALLTAGS() throws IOException {
	InputStream inputStream2 = this.getResources().getAssets()
			.open("ALLTOPICSS.txt");
	BufferedReader reader2 = new BufferedReader(new InputStreamReader(
			inputStream2));

	String line = null;
	String expectedLine = "";
	int counter = 0;
	int counter1 = 0;
	int number = 1;
	while ((line = reader2.readLine()) != null) {
		expectedLine=expectedLine+line.replaceAll("\\d", "")+"\n";
	}

	return expectedLine;

}
public String ALLTOPICS() throws IOException {
	InputStream inputStream2 = this.getResources().getAssets()
			.open("ALLTOPICS");
	BufferedReader reader2 = new BufferedReader(new InputStreamReader(
			inputStream2));

	String line = null;
	String expectedLine = "";
	int counter = 0;
	int counter1 = 0;
	int number = 1;
	while ((line = reader2.readLine()) != null) {
		expectedLine=expectedLine+line+"\n";
		counter++;
		if(counter==200){
			expectedLine=expectedLine+"CLICK HERE FOR MORE";
			break;
		}
	}

	return expectedLine;

}
public String ALLCOMMENT() throws IOException {
	InputStream inputStream2 = this.getResources().getAssets()
			.open("COMMENTS.txt");
	BufferedReader reader2 = new BufferedReader(new InputStreamReader(
			inputStream2));

	String line = null;
	String expectedLine = "";
	int counter = 0;
	int counter1 = 0;
	int number = 1;
	while ((line = reader2.readLine()) != null) {
		expectedLine=expectedLine+line+"\n";
		
	}

	return expectedLine;

}
@Override
protected void onPause() {
	super.onPause();
	//finish();
}
}
