package com.gloryproject.renewingmymind;

import com.parse.Parse;
import com.parse.ParseObject;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class Biblepopup extends Activity {
	TextView tv,tv1;
@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
    Parse.initialize(this, "e5sayD2kSIpaEuB5lqmIjS7sX9v6uihOVBAkUAhN", "Prg8islTdiOwbjmdC1G1hIynRss6RgsVUjk84M1S");

	setContentView(R.layout.biblepopupview);
	
	tv=(TextView) findViewById(R.id.popupview);
	Button b=(Button) findViewById(R.id.EXIT);
	Button b1=(Button) findViewById(R.id.addressthought);
	
	
	final String thought=getIntent().getStringExtra("thought");
	String data = getIntent().getStringExtra("VERSE");
	Uri uri=getIntent().getData();
	
	
	if(uri!=null){
		finish();
	}
	if(data!=null){
		tv.setText(data.toString());
	}
	b.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			finish();
            
			
		}
	});
	if(thought!=null){
	tv1=(TextView) findViewById(R.id.addressedthought);	
	tv1.setText(thought);
	}
	b1.setOnClickListener(new OnClickListener() {
		
		private SharedPreferences sharedpreferencesuser;

		@Override
		public void onClick(View arg0) {
			sharedpreferencesuser = getSharedPreferences("SIGNINDETAILS", Context.MODE_PRIVATE);

			if(thought!=null){
			String thethought=thought.substring(0,thought.indexOf("."));
			String verse=tv.getText().toString().substring(0,
					tv.getText().toString().indexOf(" "));
			ParseObject thoughtverse=new ParseObject("THOUGHTVERSE");
			thoughtverse.put("username", sharedpreferencesuser.getString("username", ""));
			thoughtverse.put("thoughtverse", thethought+"."+verse);
			thoughtverse.saveEventually();
			}
		}
	});
}
@Override
protected void onPause() {
	super.onPause();
	//finish();
}

}
