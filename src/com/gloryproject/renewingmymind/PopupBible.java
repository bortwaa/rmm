package com.gloryproject.renewingmymind;



import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.List;

import android.app.Activity;
import android.app.SearchManager;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;

public class PopupBible extends Activity {
	Button b,b0,b1,b2;
@Override
protected void onCreate(Bundle savedInstanceState) {
	super.onCreate(savedInstanceState);
	setContentView(R.layout.popupbible);
	
	startService();
	b=(Button) findViewById(R.id.switchon);
	b0=(Button) findViewById(R.id.bibleok);
	b1=(Button) findViewById(R.id.switchoff);
	b2=(Button) findViewById(R.id.mymainmenu);
	TextView tv=(TextView) findViewById(R.id.displaytheverse);
	
	String key=getIntent().getStringExtra("thisclip");
	if(key!=null){
		if(key.contains(":")){
			try {
				if(key.contains(".")){key=key.replace(".", "");}
				tv.setText(readBible(key));
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}else{
			//tv.setText(readSynonyms("ThessFinalAutodetect", key));
Intent i=new Intent(Intent.ACTION_SEARCH);
i.setPackage("livio.pack.lang.en_US");
i.putExtra(SearchManager.QUERY, key);
List lri=getApplicationContext().getPackageManager().queryIntentActivities(i, 
		PackageManager.MATCH_DEFAULT_ONLY);
if((lri!=null)&&(lri.size()>0))
	startActivity(i);
else Log.d("demo", "Intent is not available");
			
			
		}
	}
	
	b.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			startService();
			
		}
	});
b0.setOnClickListener(new OnClickListener() {
		
		@Override
		public void onClick(View arg0) {
			//onBackPressed();
			finish();
			//System.exit(0);
			
		}
	});
b1.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View arg0) {
		stopService();
		finish();
		System.exit(0);
		
	}
});
b2.setOnClickListener(new OnClickListener() {
	
	@Override
	public void onClick(View arg0) {
		//Intent i=new Intent(PopupBible.this,MENU.class);
		//startActivity(i);
		
	}
});

	
}
public void startService() {
    startService(new Intent(getBaseContext(), Myservice.class));
 }
public void stopService() {
    stopService(new Intent(getBaseContext(), Myservice.class));
 }
public  String readBible(String expectedLine) throws IOException {
	if(expectedLine.contains(",")){expectedLine=expectedLine.replaceAll(",", "");}

	if(expectedLine.contains(".")){
	expectedLine=expectedLine.substring(expectedLine.indexOf(".")+1);
	}
	if(expectedLine.contains(")")){
		expectedLine=expectedLine.substring(expectedLine.indexOf(")")+1);
		}
	allocation allot=new allocation();
	String bibleverse=allot.books(expectedLine);
	String fullverse="";
	if(bibleverse.contains("-")){
		fullverse=bible(bibleverse.substring(0,bibleverse.indexOf(".")), 
				bibleverse.substring(bibleverse.indexOf(".")+1,bibleverse.indexOf(":")), 
				bibleverse.substring(bibleverse.indexOf(":")+1,bibleverse.indexOf("-")),
				bibleverse.substring(bibleverse.indexOf("-")+1));	
	}else{
		fullverse=bible(bibleverse.substring(0,bibleverse.indexOf(".")), 
				bibleverse.substring(bibleverse.indexOf(".")+1,bibleverse.indexOf(":")), 
				bibleverse.substring(bibleverse.indexOf(":")+1),
				"");
	}
	return fullverse;
}
public  String bible(String BOOK,String CHAPTER,String VERSE,String END) throws IOException{
	InputStream inputStream = this.getResources().getAssets().open(BOOK+".txt");
	BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	
	int linecount=0;
	String expectedLine="";
	String line="";
	int counter=0;
	int limiter=0;
	boolean begin=false;
	 boolean endverse=false;
if(!END.equals("")){limiter=Integer.parseInt(END)-Integer.parseInt(VERSE);}
	 while ((line = reader.readLine()) != null) {
     	linecount++;
       if(BOOK.toLowerCase().contains("psal")){
     	  
     	  if(line.contains("PSALM "+CHAPTER))
 {   
     		  begin=true;
 }
     		  if(begin==true){
     		  if(line.trim().startsWith(VERSE+" ")){
     			  
	 expectedLine=BOOK+CHAPTER+":"+line;break;
	 
	 
     		  }
 }

     		  
     	      }  
       else{
       if(line.contains("CHAPTER "+CHAPTER))
       {   
    	 begin=true;  
    	  //expectedLine=line;break; 
       
     }
      
       if(begin==true){
       if(line.trim().startsWith(VERSE+" ")||endverse==true)
       {
    	   if(END.equals(""))
    	   {expectedLine=BOOK+CHAPTER+":"+line;break;}
    	   else
    	   {
    		   endverse=true;
    		   expectedLine=expectedLine+line;
    		  counter++;
    		  if(counter==limiter+1){
    			  expectedLine=BOOK+CHAPTER+":"+expectedLine;
    			  begin=false;break;
    		  }
    	   }
    		   
    	   
       }
       
       }
	 }
}
	 return expectedLine;	 
}
public void broadcastIntent(String value){
	Intent intent=new Intent();
	intent.setAction("com.turorialspoint.CUSTOM_INTENT");
	
	
	intent.putExtra("theclip", value);
	sendBroadcast(intent);
}
public String readSynonyms(String filename,String search) throws IOException{
	InputStream inputStream = this.getResources().getAssets().open(filename);
	BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
	
	String line="";
	String searchkey=search.toUpperCase();
    String expectedLine=""; 
        while ((line = reader.readLine()) != null) {
        	if(line.toUpperCase().contains(searchkey.trim())){
        		 expectedLine=line;
        		 break;
            }
         
          
        }
		return expectedLine;
}
@Override
protected void onPause() {
	super.onPause();
	finish();
}
}
