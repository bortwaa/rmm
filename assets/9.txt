9a)Mr 16:16 He that believeth and is baptized shall be saved; but he that believeth not shall be damned.
9b)Joh 1:12 But as many as received him, to them gave he power to become the sons of God, even to them that believe on his name:
9c)Joh 3:16 For God so loved the world, that he gave his only begotten Son, that whosoever believeth in him should not perish, but have everlasting life.
9d)Ro 1:16  For I am not ashamed of the gospel of Christ: for it is the power of God unto salvation to every one that believeth; to the Jew first, and also to the Greek.
9e)Eph 2:8 For by grace are ye saved through faith; and that not of yourselves: it is the gift of God: 