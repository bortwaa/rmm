211a)2Co 5:17 Therefore if any man be in Christ, he is a new creature: old things are passed away; behold, all things are become new.
211b)Ro 8:10  And if Christ be in you, the body is dead because of sin; but the Spirit is life because of righteousness.
211c)Ro 8:12 Therefore, brethren, we are debtors, not to the flesh, to live after the flesh.
211d)Ga 5:1  Stand fast therefore in the liberty wherewith Christ hath made us free, and be not entangled again with the yoke of bondage.
211e)Heb 9:26 For then must he often have suffered since the foundation of the world: but now once in the end of the world hath he appeared to put away sin by the sacrifice of himself.
211f)Tit 2:11  For the grace of God that bringeth salvation hath appeared to all men,
211g)1Pe 2:9 But ye are a chosen generation, a royal priesthood, an holy nation, a peculiar people; that ye should shew forth the praises of him who hath called you out of darkness into his marvellous light:
211h)Php 2:12 Wherefore, my beloved, as ye have always obeyed, not as in my presence only, but now much more in my absence, work out your own salvation with fear and trembling.\ God has already changed me in the inside so that by faith I should work out this change to the outside.
