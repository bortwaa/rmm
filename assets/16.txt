16a)Joh 3:36 He that believeth on the Son hath everlasting life: and he that believeth not the Son shall not see life; but the wrath of God abideth on him.
16b)Joh 5:24 Verily, verily, I say unto you, He that heareth my word, and believeth on him that sent me, hath everlasting life, and shall not come into condemnation; but is passed from death unto life.
16c)Ro 8:14 For as many as are led by the Spirit of God, they are the sons of God.15 For you have not received the spirit of bondage again to fear, but you have received the Spirit of adoption by which we cry, Abba, Father!16 The Spirit Himself bears witness with our spirit that we are the children of God.(MKJV)
16d)2Co 5:17 Therefore if any man be in Christ, he is a new creature: old things are passed away; behold, all things are become new.
16e)1Jo 1:9 If we confess our sins, he is faithful and just to forgive us our sins, and to cleanse us from all unrighteousness.
16f)1Jo 2:3  And hereby we do know that we know him, if we keep his commandments.
16g)1Jo 3:15 Whosoever hateth his brother is a murderer: and ye know that no murderer hath eternal life abiding in him.
16h)1Jo 5:13 These things have I written unto you that believe on the name of the Son of God; that ye may know that ye have eternal life, and that ye may believe on the name of the Son of God