Rom 3:28\ My deeds do not accurately determine who I am, but my faith in Christ is what accurately determines who I am and thus what I can do. The measure of my faith will lead to the same measure of deeds.

