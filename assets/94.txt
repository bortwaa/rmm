94a)2Ti 2:15 Study to shew thyself approved unto God, a workman that needeth not to be ashamed, rightly dividing the word of truth. \Approval from God is greater than that of men and cannot be sought while seeking the approval of men.
94b)Ac 2:22 Ye men of Israel, hear these words; Jesus of Nazareth, a man approved of God among you by miracles and wonders and signs, which God did by him in the midst of you, as ye yourselves also know: \Approval from God will end up in approval from men.
94c)Heb 2:4 God also bearing them witness, both with signs and wonders, and with divers miracles, and gifts of the Holy Ghost, according to his own will \God will make men approve you for the sake of His will and not your own sake.
94d)Joh 12:43 For they loved the praise of men more than the praise of God.
94e)Joh 5:44 How can ye believe, which receive honour one of another, and seek not the honour that cometh from God only \It is a great honour to desire the God's approval above the approval of men.
94f)Mt 10:26 Fear them not therefore: for there is nothing covered, that shall not be revealed; and hid, that shall not be known. \In the end, all men shall see your true value and worth.
94g)Mt 6:4 That thine alms may be in secret: and thy Father which seeth in secret himself shall reward thee openly. The extent you sacrifice the approval of men for the approval of God, to that same extent God will make men approve you in the end.
94h)1Pe 5:6 Humble yourselves therefore under the mighty hand of God, that he may exalt you in due time:
94i)Mt 23:12 And whosoever shall exalt himself shall be abased; and he that shall humble himself shall be exalted.
94j)Mt 18:4 Whosoever therefore shall humble himself as this little child, the same is greatest in the kingdom of heaven.