Hebrew 11:39\faith might seem to fail in delivering expected results but can never fail to give us a good report before God which is far greater than what is expected.
Rom 10:17\faith is not driven by results but by the word of God
2Thess 1:3\The failure of our faith to produce expected results is a call to grow in faith and not forsake the way of faith
Hebrew 10:35-36
Gal 6:9
Rom 12:3\The measure of results of faith is in the measure of faith
