215a)Php 4:13 I can do all things through Christ which strengtheneth me. Christ is to me, positve change.
215b)Mr 9:23  Jesus said unto him, If thou canst believe, all things are possible to him that believeth. \My change is at the mercy of my faith.
215c)2Co 3:18 But we all, with open face beholding as in a glass the glory of the Lord, are changed into the same image from glory to glory, even as by the Spirit of the Lord. \To the degree my focus and faith on God's Spirit is unchanging, to the same degree I will change.
215d)2Co 3:17 Now the Lord is that Spirit: and where the Spirit of the Lord is, there is liberty. \There are infinite possibilities of change because the Spirit of change lives in and works in me by faith.
215e)2Ti 1:7 For God hath not given us the spirit of fear; but of power, and of love, and of a sound mind. \God's Spirit in me has enough love and power to transform my form into God's own form as I allow Him to keep my mind sound by faith.
215f)2Co 5:17 Therefore if any man be in Christ, he is a new creature: old things are passed away; behold, all things are become new. \To the extent my life is in Christ by faith; to that same extent I will experience change.
215g)Php 2:13 For it is God which worketh in you both to will and to do of his good pleasure.\ Change is not self-produced but God-produced through faith.
215h)1Co 4:20 For the kingdom of God is not in word, but in power. \The rule of God in my life through faith always brings great changes.
215i)2Co 4:16 For which cause we faint not; but though our outward man perish, yet the inward man is renewed day by day. \The life of faith is an ever changing forward moving life where the bad things perish and the good things are ever renewed; changes in both directions.
215j)Pr 23:7 For as he thinketh in his heart, so is he:...  \Change is at the mercy of my thought and not at my being tough.
215k)Pr 4:18 But the path of the just is as the shining light, that shineth more and more unto the perfect day.
215l)2Co 3:18 But we all, with open face beholding as in a glass the glory of the Lord, are changed into the same image from glory to glory, even as by the Spirit of the Lord. 
215m)Col 3:10 And have put on the new man, which is renewed in knowledge after the image of him that created him: \My life is programmed in God unto progressive positive change through the progressive knowledge or faith in Christ.
