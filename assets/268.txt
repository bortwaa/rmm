46.2Cor 5:17   \Through my faith In Christ Jesus, everything about my life of sin is replaced with everything about Christ's life of righteousness with all His privileges in heaven and on earth
a.2Co 8:9  
b.2Co 5:21  \ Christ who deserves every good privilege and nothing evil became every evil thing so that I will be made deserving of all good privileges
c.1Ti 1:15 This is a faithful saying, and worthy of all acceptation, that Christ Jesus came into the world to save sinners; of whom I am chief.\Paul who called himself the greatest of all sinners received the greatest of all privileges (even an apostle of Jesus Christ)
d.Luke 22:34 And he said, I tell thee, Peter, the cock shall not crow this day, before that thou shalt thrice deny that thou knowest me. Matt 10:33, Matt 16:18-19 ,  \Peter received one of the greatest privileges of the kingdom though he committed one of the greatest sins against the kingdom
