132a)Ps 37:4 Delight thyself also in the LORD; and he shall give thee the desires of thine heart. \ True freedom to live out the genuine desires of my heart is gotten by allowing God into my life.
132b)Php 2:13 For it is God which worketh in you both to will and to do of his good pleasure. God empowers my will and actions to serve their desired purposes as I surrender to Him.
132c)Joh 10:10 The thief cometh not, but for to steal, and to kill, and to destroy: I am come that they might have life, and that they might have it more abundantly.  \Surrendering to God is the key/freedom to living life to the fullest.
132d)Php 4:13 I can do all things through Christ which strengtheneth me.  \Surrendering to God is not slavery to God but strength to do all things.
132e)Joh 16:23  And in that day ye shall ask me nothing. Verily, verily, I say unto you, Whatsoever ye shall ask the Father in my name, he will give it you. \God is interested in fulfilling the desires of those who surrender to Him.
132f)Mr 12:29 And Jesus answered him, The first of all the commandments is, Hear, O Israel; The Lord our God is one Lord:  God is the owner and originator of the desires of my heart. \It is God Himself that gives desires. He therefore, cannot be against the genuine desires of our hearts. It is only in surrendering to Him that I will have them fulfilled.

