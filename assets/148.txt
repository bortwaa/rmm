148a)Eph 1:6 To the praise of the glory of his grace, wherein he hath made us accepted in the beloved.
148b)Ps 27:10 When my father and my mother forsake me, then the LORD will take me up.
148c)Isa 49:15 Can a woman forget her sucking child, that she should not have compassion on the son of her womb yea, they may forget, yet will I not forget thee.
148d)Isa 49:16 Behold, I have graven thee upon the palms of my hands; thy walls are continually before me. \God's acceptance and love is what really matters to me.
148e)Heb 10:25 Not forsaking the assembling of ourselves together, as the manner of some is; but exhorting one another: and so much the more, as ye see the day approaching.
148f)Mr 4:17 And have no root in themselves, and so endure but for a time: afterward, when affliction or persecution ariseth for the word's sake, immediately they are offended. \I benefit more from the church than offences from it. I should therefore not allow offences to rob of these benefits but rather have strong roots in the church.
148g)1Th 3:3 That no man should be moved by these afflictions: for yourselves know that we are appointed thereunto.  \Affliction is the evidence of and way to greater appointments.
148h)2Ti 3:12 Yea, and all that will live godly in Christ Jesus shall suffer persecution. \Offences are devilish strategies to take me out of church and therefore, out of godliness
148i)Ro 8:35 Who shall separate us from the love of Christ shall tribulation, or distress, or persecution, or famine, or nakedness, or peril, or sword \My love for Christ is proven to be genuine if nothing is able to keep me away from His body which is the church.

